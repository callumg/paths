CREATE DATABASE IF NOT EXISTS path_testing;

USE path_testing;

/* CREATE ADMIN USER */
CREATE USER IF NOT EXISTS 'dbadmin'@'localhost' IDENTIFIED BY 'root';

/* GRANT PERMISSIONS */
GRANT ALL PRIVILEGES ON path.* TO 'dbadmin'@'localhost';