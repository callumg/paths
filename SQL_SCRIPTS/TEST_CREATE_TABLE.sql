/* CREATE DATABASE paths */
CREATE DATABASE IF NOT EXISTS path_testing;

/* GO INTO DATABASE paths */
USE path_testing;

/* CREATE USER TABLE */
CREATE TABLE IF NOT EXISTS user (
    username VARCHAR(64) PRIMARY KEY NOT NULL,
    password TEXT NOT NULL,
    salt BLOB NOT NULL
);

/* CREATE GOAL TABLE */
CREATE TABLE IF NOT EXISTS goal (
	goal_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
	num_val INT(11),
    str_val VARCHAR(64),
    goal_type VARCHAR(64) NOT NULL
);

/* CREATE ACTION TABLE */
CREATE TABLE IF NOT EXISTS action (
	action_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
	num_val INT(11),
    str_val VARCHAR(64),
    action_type VARCHAR(64) NOT NULL
);

/* CREATE PASSAGE TABLE */
CREATE TABLE IF NOT EXISTS passage (
	passage_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(64) NOT NULL,
    content VARCHAR(64) NOT NULL
);

/* CREATE LINK TABLE */
CREATE TABLE IF NOT EXISTS link (
	link_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    text VARCHAR(64) NOT NULL,
    reference VARCHAR(64) NOT NULL,
    passage_id INT(11) UNSIGNED,
    FOREIGN KEY (passage_id) REFERENCES passage(passage_id)
);

/* CREATE PLAYER TABLE */
CREATE TABLE IF NOT EXISTS player (
	player_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    health INT(4) UNSIGNED NOT NULL,
    score INT(4) UNSIGNED NOT NULL,
    gold INT(4) UNSIGNED NOT NULL,
    inventory TEXT NOT NULL
);

/* CREATE STORY TABLE */
CREATE TABLE IF NOT EXISTS story (
    story_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(64) NOT NULL,
    opening_passage INT(11) UNSIGNED NOT NULL,
    FOREIGN KEY (opening_passage) REFERENCES passage(passage_id)
);

/* CREATE STORY PASSAGE MAPPING TABLE */
CREATE TABLE IF NOT EXISTS story_passage (
    id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
	story_id INT(11) UNSIGNED,
	passage_id INT(11) UNSIGNED,
	link_id INT(11) UNSIGNED,
    FOREIGN KEY (story_id) REFERENCES story(story_id),
    FOREIGN KEY (passage_id) REFERENCES passage(passage_id),
    FOREIGN KEY (link_id) REFERENCES link(link_id)
);

/* CREATE GAME TABLE */
CREATE TABLE IF NOT EXISTS game (
	game_id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    player_id INT(11) UNSIGNED NOT NULL,
    story_id INT(11) UNSIGNED NOT NULL,
    username VARCHAR(64),
    FOREIGN KEY (player_id) REFERENCES player(player_id),
    FOREIGN KEY (story_id) REFERENCES story(story_id),
    FOREIGN KEY (username) REFERENCES user(username)
);

/* CREATE GAME GOAL TABLE */
CREATE TABLE IF NOT EXISTS game_goal (
	game_id INT(11) UNSIGNED,
	goal_id INT(11) UNSIGNED,
	FOREIGN KEY (game_id) REFERENCES game(game_id),
    FOREIGN KEY (goal_id) REFERENCES goal(goal_id),
	PRIMARY KEY (game_id, goal_id)
);

/* CREATE LINK ACTION TABLE */
CREATE TABLE IF NOT EXISTS link_action (
	link_id INT(11) UNSIGNED NOT NULL,
	action_id INT(11) UNSIGNED NOT NULL,
	FOREIGN KEY (link_id) REFERENCES link(link_id),
    FOREIGN KEY (action_id) REFERENCES action(action_id),
    PRIMARY KEY (link_id, action_id)
);

/* CREATE ADMIN USER */
CREATE USER IF NOT EXISTS 'dbadmin'@'localhost' IDENTIFIED BY 'root';

/* GRANT PERMISSIONS */
GRANT ALL PRIVILEGES ON path_testing.* TO 'dbadmin'@'localhost';

/* CREATE TEST DATA */
INSERT INTO user (username, password, salt) VALUES ('guest', 'none', 'none');