package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.User;

public class UserTest {
    
    @Nested
    class UserConstructor {
        byte []salt;
        User user;

        @BeforeEach
        void setUp() {
            salt = new byte[16];
        }
        
        @Test
        void testUserConstructorWithNullUsername() {
            try {
                user = new User(null, "Test", salt);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The username cannot be null.", e.getMessage());
            }
        }

        @Test
        void testUserConstructorWithNullPassword() {
            try {
                user = new User("Test", null, salt);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The hashed password cannot be null.", e.getMessage());
            }
        }

        @Test
        void testUserConstructorWithNullSalt() {
            try {
                user = new User("Test", "Test", null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The salt cannot be null.", e.getMessage());
            }
        }

        @Test
        void testUserConstructorWithEmptyUsername() {
            try {
                user = new User("", "Test", salt);
            } catch (IllegalArgumentException e) {
                assertEquals("The username cannot be empty.", e.getMessage());
            }
        }

        @Test
        void testUserConstructorWithEmptyHash() {
            try {
                user = new User("Test", "", salt);
            } catch (IllegalArgumentException e) {
                assertEquals("The hashed password cannot be empty.", e.getMessage());
            }
        }
    }

    @Nested
    class UserSetters {
        User user;

        @BeforeEach
        void setUp() {
            user = new User("Test", "Test", new byte[16]);
        }

        @Test
        void testSetUsernameWithNull() {
            try {
                user.setUsername(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The username cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetUsernameWithEmpty() {
            try {
                user.setUsername("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The username cannot be empty.", e.getMessage());
            }
        }

        @Test
        void testSetHashedPasswordWithNull() {
            try {
                user.setHashedPassword(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The hashed password cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetHashedPasswordWithEmpty() {
            try {
                user.setHashedPassword("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The hashed password cannot be empty.", e.getMessage());
            }
        }

        @Test
        void testSetSaltWithNull() {
            try {
                user.setSalt(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The salt cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetGamesWithNull() {
            try {
                user.setGames(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The games cannot be null.", e.getMessage());
            }
        }
    }

    @Nested
    class UserMethods {
        User user;

        @BeforeEach
        void setUp() {
            user = new User("Test", "Test", new byte[16]);
        }

        @Test
        void testAddGameWithNull() {
            try {
                user.addGame(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The game cannot be null.", e.getMessage());
            }
        }

        @Test
        void testRemoveGameWithNull() {
            try {
                user.removeGame(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("The game cannot be null.", e.getMessage());
            }
        }
    }
}
