package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddInventoryItemAction;
import edu.ntnu.idatt2001.callumg.model.actions.RemoveInventoryItemAction;

public class InventoryActionTest {

    @Nested
    class AddInventoryItemActionTests {
        Player player;
        AddInventoryItemAction action;
        
        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddInventoryItemAction() {
            action = new AddInventoryItemAction("Test");
            action.execute(player);
            assertEquals(1, player.getInventory().size());
            assertEquals("Test", player.getInventory().get(player.getInventory().size() - 1));
        }

        @Test
        void testAddInventoryItemActionWithNullItem() {
            try {
                action = new AddInventoryItemAction(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Value cannot be null.", e.getMessage());
            }
        }

        @Test
        void testAddInventoryItemActionWithEmptyItem() {
            try {
                action = new AddInventoryItemAction("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be blank.", e.getMessage());
            }
        }
    }

    @Nested
    class RemoveInventoryItemActionTests {
        Player player;
        RemoveInventoryItemAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveInventoryItemAction() {
            player.getInventory().add("Test");
            action = new RemoveInventoryItemAction("Test");
            action.execute(player);
            assertEquals(0, player.getInventory().size());
        }

        @Test
        void testRemoveInventoryItemActionWithNullItem() {
            try {
                action = new RemoveInventoryItemAction(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Value cannot be null.", e.getMessage());
            }
        }

        @Test
        void testRemoveInventoryItemActionWithEmptyItem() {
            try {
                action = new RemoveInventoryItemAction("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be blank.", e.getMessage());
            }
        }
    }
}
