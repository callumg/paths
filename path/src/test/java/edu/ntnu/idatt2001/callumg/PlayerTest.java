package edu.ntnu.idatt2001.callumg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class PlayerTest {

    Player player;

    @Nested
    class PlayerConstructor {
        @Test
        void testConstructor() {
            player = new Player("Test", 10, 10, 10);
            assertEquals("Test", player.getName());
            assertEquals(10, player.getHealth());
            assertEquals(10, player.getScore());
            assertEquals(10, player.getGold());
        }

        @Test
        void testConstructorWithNullName() {
            try {
                player = new Player(null, 10, 10, 10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be null.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithBlankName() {
            try {
                player = new Player("", 10, 10, 10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithNegativeHealth() {
            try {
                player = new Player("Test", -1, 10, 10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Health cannot be negative.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithNegativeScore() {
            try {
                player = new Player("Test", 10, -1, 10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Score cannot be negative.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithNegativeGold() {
            try {
                player = new Player("Test", 10, 10, -1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Gold cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerSetters {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testSetName() {
            player.setName("Test2");
            assertEquals("Test2", player.getName());
        }

        @Test
        void testSetHealth() {
            player.setHealth(20);
            assertEquals(20, player.getHealth());
        }

        @Test
        void testSetScore() {
            player.setScore(20);
            assertEquals(20, player.getScore());
        }

        @Test
        void testSetGold() {
            player.setGold(20);
            assertEquals(20, player.getGold());
        }

        @Test
        void testSetNullName() {
            try {
                player.setName(null);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetBlankName() {
            try {
                player.setName("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testSetNegativeHealth() {
            try {
                player.setHealth(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Health cannot be negative.", e.getMessage());
            }
        }

        @Test
        void testSetNegativeScore() {
            try {
                player.setScore(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Score cannot be negative.", e.getMessage());
            }
        }

        @Test
        void testSetNegativeGold() {
            try {
                player.setGold(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Gold cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerAddScore {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddScore() {
            player.addScore(10);
            assertEquals(20, player.getScore());
        }

        @Test
        void testAddNegativeScore() {
            try {
                player.addScore(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Score cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerAddGold {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddGold() {
            player.addGold(10);
            assertEquals(20, player.getGold());
        }

        @Test
        void testAddNegativeGold() {
            try {
                player.addGold(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Gold cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerAddHealth {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddHealth() {
            player.addHealth(10);
            assertEquals(20, player.getHealth());
        }

        @Test
        void testAddNegativeHealth() {
            try {
                player.addHealth(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Health cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerRemoveScore {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveScore() {
            player.removeScore(10);
            assertEquals(0, player.getScore());
        }

        @Test
        void testRemoveNegativeScore() {
            try {
                player.removeScore(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Score cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerRemoveGold {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveGold() {
            player.removeGold(10);
            assertEquals(0, player.getGold());
        }

        @Test
        void testRemoveNegativeGold() {
            try {
                player.removeGold(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Gold cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerRemoveHealth {
        Player player;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveHealth() {
            player.removeHealth(10);
            assertEquals(0, player.getHealth());
        }

        @Test
        void testRemoveNegativeHealth() {
            try {
                player.removeHealth(-1);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Health cannot be negative.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerAddItem {
        Player player;
        String item;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
            item = "Test";
        }

        @Test
        void testAddItem() {
            player.addToInventory(item);
            assertEquals(1, player.getInventory().size());
        }

        @Test
        void testAddNullItem() {
            try {
                player.addToInventory(null);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Item cannot be null.", e.getMessage());
            }
        }

        @Test
        void testAddBlankItem() {
            try {
                player.addToInventory("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Item cannot be blank.", e.getMessage());
            }
        }
    }

    @Nested
    class PlayerRemoveItem {
        Player player;
        String item;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
            item = "Test";
        }

        @Test
        void testRemoveItem() {
            player.addToInventory(item);
            player.removeFromInventory(item);
            assertEquals(0, player.getInventory().size());
        }

        @Test
        void testRemoveNullItem() {
            try {
                player.removeFromInventory(null);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Item cannot be null.", e.getMessage());
            }
        }

        @Test
        void testRemoveBlankItem() {
            try {
                player.removeFromInventory("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Item cannot be blank.", e.getMessage());
            }
        }
    }
}
