package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddHealthAction;
import edu.ntnu.idatt2001.callumg.model.actions.RemoveHealthAction;

public class HealthActionTest {

    @Nested
    class AddHealthActionTests {
        Player player;
        AddHealthAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddHealthAction() {
            action = new AddHealthAction(10);
            action.execute(player);
            assertEquals(20, player.getHealth());
        }

        @Test
        void testAddHealthActionWithNegativeHealth() {
            try {
                action = new AddHealthAction(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class RemoveHealthActionTests {

        Player player;
        RemoveHealthAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveHealthAction() {
            action = new RemoveHealthAction(10);
            action.execute(player);
            assertEquals(0, player.getHealth());
        }

        @Test
        void testRemoveHealthActionWithNegativeHealth() {
            try {
                action = new RemoveHealthAction(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }
}
