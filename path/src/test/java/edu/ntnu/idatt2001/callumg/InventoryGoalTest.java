package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddInventoryItemAction;
import edu.ntnu.idatt2001.callumg.model.goals.InventoryGoal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class InventoryGoalTest {
    InventoryGoal goal;

    @Nested
    class InventoryGoalConstructor {
        @Test
        void testInventoryGoalConstructorWithNegativeInventory() {
            try {
                goal = new InventoryGoal(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class InventoryGoalIsCompleted {
        Player player;
        InventoryGoal goal;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 5, 5, 5);
        }

        @Test
        void testInventoryGoalIsCompletedWithNegativeInventory() {
            try {
                goal = new InventoryGoal(10);
                goal.isCompleted(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testInventoryGoalIsCompletedWithNotEnoughInventory() {
            goal = new InventoryGoal(10);
            assertEquals(false, goal.isCompleted(player));
        }

        @Test
        void testInventoryGoalIsCompletedWithEnoughInventory() {
            goal = new InventoryGoal(10);
            AddInventoryItemAction action = new AddInventoryItemAction("Test");
            for (int i = 0; i < 11; i++)
                action.execute(player);
            assertEquals(true, goal.isCompleted(player));
        }
    }
}
