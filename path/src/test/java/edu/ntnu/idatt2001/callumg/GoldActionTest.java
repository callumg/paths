package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.IntegerAction;
import edu.ntnu.idatt2001.callumg.model.actions.AddGoldAction;
import edu.ntnu.idatt2001.callumg.model.actions.RemoveGoldAction;

public class GoldActionTest {
    @Nested
    class AddGoldActionTests {
        Player player;
        IntegerAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddGoldAction() {
            action = new AddGoldAction(10);
            action.execute(player);
            assertEquals(20, player.getGold());
        }

        @Test
        void testAddGoldActionWithNegativeGold() {
            try {
                action = new AddGoldAction(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class RemoveGoldActionTests {
        Player player;
        IntegerAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveGoldAction() {
            action = new RemoveGoldAction(10);
            action.execute(player);
            assertEquals(0, player.getGold());
        }

        @Test
        void testRemoveGoldActionWithNegativeGold() {
            try {
                action = new RemoveGoldAction(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }
}
