package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddHealthAction;
import edu.ntnu.idatt2001.callumg.model.goals.HealthGoal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class HealthGoalTest {

    @Nested
    class HealthGoalConstructor {
        HealthGoal goal;

        @Test
        void testHealthGoalConstructorWithNegativeHealth() {
            try {
                goal = new HealthGoal(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class HealthGoalIsCompleted {
        Player player;
        HealthGoal goal;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 5, 5, 5);
        }

        @Test
        void testHealthGoalIsCompletedWithNegativeHealth() {
            try {
                goal = new HealthGoal(10);
                goal.isCompleted(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testHealthGoalIsCompletedWithNotEnoughHealth() {
            goal = new HealthGoal(10);
            assertEquals(false, goal.isCompleted(player));
        }

        @Test
        void testHealthGoalIsCompletedWithEnoughHealth() {
            goal = new HealthGoal(10);
            AddHealthAction action = new AddHealthAction(10);
            action.execute(player);
            assertEquals(true, goal.isCompleted(player));
        }
    }

}

