package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Link;
import edu.ntnu.idatt2001.callumg.model.Passage;
import edu.ntnu.idatt2001.callumg.model.Story;

public class StoryTest {
    
    @Nested
    class StoryConstructor {

        Passage passage;
        Story story;
        
        @Test
        void testConstructor() {
            passage = new Passage("Test", "Test");
            story = new Story("Test", passage);
            assertEquals("Test", story.getTitle());
            assertEquals(passage, story.getOpeningPassage());
        }

        @Test
        void testConstructorWithNullTitle() {
            try {
                passage = new Passage("Test", "Test");
                story = new Story(null, passage);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Title cannot be null.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithBlankTitle() {
            try {
                passage = new Passage("Test", "Test");
                story = new Story("", passage);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Title cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithNullAuthor() {
            try {
                story = new Story("Test", null);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Opening passage cannot be null.", e.getMessage());
            }
        }
    }

    @Nested
    class StoryPassage {
        Story story;
        Passage passage;
        Link link;

        @BeforeEach
        void setUp() {
            passage = new Passage("Test", "Test");
            story = new Story("Test", passage);
        }

        @Test
        void testAddPassage() {
            story.addPassage(passage);
            assertTrue(story.getPassages().contains(passage));
        }

        @Test
        void testAddPassageWithNull() {
            try {
                story.addPassage(null);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Passage cannot be null.", e.getMessage());
            }
        }
    }
}
