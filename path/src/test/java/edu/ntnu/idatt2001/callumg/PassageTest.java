package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Link;
import edu.ntnu.idatt2001.callumg.model.Passage;

public class PassageTest {
    
    @Nested
    class PassageConstructor {

        Passage passage;
        
        @Test
        void testConstructor() {
            passage = new Passage("Test", "Test");
            assertEquals("Test", passage.getTitle());
            assertEquals("Test", passage.getContent());
        }

        @Test
        void testConstructorWithNullTitle() {
            try {
                passage = new Passage(null, "Test");
            } catch (IllegalArgumentException e) {
                assertEquals("Title cannot be null.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithNullContent() {
            try {
                passage = new Passage("Test", null);
            } catch (IllegalArgumentException e) {
                assertEquals("Content cannot be null.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithBlankTitle() {
            try {
                passage = new Passage("", "Test");
            } catch (IllegalArgumentException e) {
                assertEquals("Title cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithBlankContent() {
            try {
                passage = new Passage("Test", "");
            } catch (IllegalArgumentException e) {
                assertEquals("Content cannot be blank.", e.getMessage());
            }
        }
    }

    @Nested
    class PassageSetters {
        
        Passage passage;

        @BeforeEach
        void setUp() {
            passage = new Passage("Test", "Test");
        }

        @Test
        void testSetTitle() {
            passage.setTitle("Test2");
            assertEquals("Test2", passage.getTitle());
        }

        @Test
        void testSetContent() {
            passage.setContent("Test2");
            assertEquals("Test2", passage.getContent());
        }

        @Test
        void testSetContentWithNull() {
            try {
                passage.setContent(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Content cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetContentWithEmptyString() {
            try {
                passage.setContent("");
            } catch (IllegalArgumentException e) {
                assertEquals("Content cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testSetTitleWithNull() {
            try {
                passage.setTitle(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Title cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetTitleWithEmptyString() {
            try {
                passage.setTitle("");
            } catch (IllegalArgumentException e) {
                assertEquals("Title cannot be blank.", e.getMessage());
            }
        }


        @Test
        void testSetLinks() {
            Link link = new Link("Test", "Test");
            List<Link> links = new ArrayList<Link>();
            links.add(link);
            passage.setLinks(links);
            assertEquals(link, passage.getLinks().get(0));
        }

        @Test
        void testSetLinksWithNull() {
            try {
                passage.setLinks(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Links cannot be null.", e.getMessage());
            }
        }
    }

    @Nested
    class PassageEquals {
        Passage passage1;
        Passage passage2;

        @BeforeEach
        void setUp() {
            passage1 = new Passage("Test", "Test");
            passage2 = new Passage("Test", "Test");
        }

        @Test
        void testEquals() {
            assertTrue(passage1.equals(passage2));
        }

        @Test
        void testEqualsWithNull() {
            assertTrue(!passage1.equals(null));
        }

        @Test
        void testEqualsWithDifferentClass() {
            assertTrue(!passage1.equals("Test"));
        }

        @Test
        void testEqualsWithDifferentTitle() {
            passage2.setTitle("Test2");
            assertTrue(!passage1.equals(passage2));
        }

        @Test
        void testEqualsWithDifferentContent() {
            passage2.setContent("Test2");
            assertTrue(!passage1.equals(passage2));
        }

        @Test
        void testEqualsWithDifferentLinks() {
            Link link = new Link("Test", "Test");
            List<Link> links = new ArrayList<Link>();
            links.add(link);
            passage2.setLinks(links);
            assertTrue(!passage1.equals(passage2));
        }
    }
}
