package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.security.NoSuchAlgorithmException;

import org.hibernate.TransactionException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.User;
import edu.ntnu.idatt2001.callumg.service.UserService;
import edu.ntnu.idatt2001.callumg.service.UserStateService;

public class UserServiceTest {

    @Nested
    class RegisterUser {
        UserService userService;
        
        @BeforeEach
        void setUp() {
            try {
                userService = new UserService();
                userService.deleteAll();
            } catch (Exception e) {
                fail();
            }
        }
        
        @Test
        void testRegisterUserWithNullUsername() {
            try {
                userService.registerUser(null, "Test");
                fail();
            } catch (NullPointerException | TransactionException | IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("The username cannot be null.", e.getMessage());
            }
        }
        
        @Test
        void testRegisterUserWithNullPassword() {
            try {
                userService.registerUser("Test", null);
                fail();
            } catch (NullPointerException | TransactionException | IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("The password cannot be null.", e.getMessage());
            }
        }
        
        @Test
        void testRegisterUserWithEmptyUsername() {
            try {
                userService.registerUser("", "Test");
                fail();
            } catch (IllegalArgumentException | TransactionException | NoSuchAlgorithmException | NullPointerException e) {
                assertEquals("The username cannot be blank.", e.getMessage());
            }
        }
        
        @Test
        void testRegisterUserWithEmptyPassword() {
            try {
                userService.registerUser("Test", "");
                fail();
            } catch (IllegalArgumentException | TransactionException | NoSuchAlgorithmException | NullPointerException e) {
                assertEquals("The password cannot be blank.", e.getMessage());
            }
        }
        
        @Test
        void testRegisterUserWithValidInput() {
            try {
                userService.registerUser("Test", "Test");
            } catch (IllegalArgumentException | NullPointerException | NoSuchAlgorithmException | TransactionException e) {
                fail();
            }
        }
    }

    @Nested
    class RemoveUser {
        UserService userService;

        @BeforeEach
        void setUp() {
            try {
                userService = new UserService();
                userService.deleteAll();
            } catch (Exception e) {
                fail();
            }
        }

        @AfterAll
        static void tearDown() {
            UserService userService;
            try {
                userService = new UserService();
                userService.deleteAll();
            } catch (Exception e) {
                fail();
            }
        }
        
        @Test
        void testRemoveUserWithNullUser() {
            try {
                userService.remove(null);
                fail();
            } catch (NullPointerException | TransactionException e) {
                assertEquals("The object to remove cannot be null.", e.getMessage());
            }
        }
        
        @Test
        void testRemoveUserWithNullUsername() {
            try {
                userService.removeById(null);
                fail();
            } catch (IllegalArgumentException | NullPointerException | TransactionException e) {
                assertEquals("The username cannot be null.", e.getMessage());
            }
        }

        @Test
        void testRemoveUserWithEmptyUsername() {
            try {
                userService.removeById("");
                fail();
            } catch (IllegalArgumentException | NullPointerException | TransactionException e) {
                assertEquals("The username cannot be blank.", e.getMessage());
            }
        }
        
        @Test
        void testRemoveUserWithValidInput() {
            try {
                User user =  userService.registerUser("Test", "Test");
                userService.removeById(user.getUsername());
            } catch (IllegalArgumentException | NullPointerException | TransactionException | NoSuchAlgorithmException e) {
                fail();
            }
        }

        @Test
        void testRemoveUserWithValidObject() {
            try {
                User user =  userService.registerUser("Test", "Test");
                userService.remove(user);
            } catch (IllegalArgumentException | NullPointerException | TransactionException | NoSuchAlgorithmException e) {
                fail();
            }
        }
    }

    @Nested
    class UserLogin {
        UserService userService;

        @BeforeEach
        void setUp() {
            try {
                userService = new UserService();
                userService.deleteAll();
                userService.registerUser("guest", "guest");
                userService.registerUser("admin", "admin");
            } catch (Exception e) {
                fail();
            }
        }

        @Test
        void testUserLoginWithNullUsername() {
            try {
                userService.logIn(null, "Test");
                fail();
            } catch (NullPointerException | IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("The username cannot be null.", e.getMessage());
            }
        }

        @Test
        void testUserLoginWithNullPassword() {
            try {
                userService.logIn("Test", null);
                fail();
            } catch (NullPointerException | IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("The password cannot be null.", e.getMessage());
            }
        }

        @Test
        void testUserLoginWithEmptyUsername() {
            try {
                userService.logIn("", "Test");
                fail();
            } catch (IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("The username cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testUserLoginWithEmptyPassword() {
            try {
                userService.logIn("Test", "");
                fail();
            } catch (IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("The password cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testUserLoginWithValidInput() {
            try {
                User user = userService.logIn("guest", "guest");
                assertEquals("guest", user.getUsername());
                assertEquals("guest", UserStateService.getCurrentUser().getUsername());
            } catch (IllegalArgumentException | NoSuchAlgorithmException e) {
                fail();
            }
        }

        @Test
        void testUserLoginWithInvalidUsername() {
            try {
                userService.logIn("test", "guest");
                fail();
            } catch (IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("User does not exist.", e.getMessage());
            }
        }

        @Test
        void testUserLoginWithInvalidPassword() {
            try {
                userService.logIn("guest", "test");
                fail();
            } catch (IllegalArgumentException | NoSuchAlgorithmException e) {
                assertEquals("Incorrect password.", e.getMessage());
            }
        }

        @Test
        void testLogOut() {
            try {
                userService.logIn("admin", "admin");
                assertEquals("admin", UserStateService.getCurrentUser().getUsername());
                UserService.logOut();
                assertEquals("guest", UserStateService.getCurrentUser().getUsername());
            } catch (IllegalArgumentException | NoSuchAlgorithmException e) {
                fail();
            }
        }
    }
}
