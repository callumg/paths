package edu.ntnu.idatt2001.callumg;

import org.junit.jupiter.api.Nested;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Game;
import edu.ntnu.idatt2001.callumg.model.Link;
import edu.ntnu.idatt2001.callumg.model.Passage;
import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.Story;
import edu.ntnu.idatt2001.callumg.model.goals.Goal;
import edu.ntnu.idatt2001.callumg.model.goals.ScoreGoal;

public class GameTest {
    
    @Nested
    class GameConstructor {
        
        Game game;

        @Test
        void testGameConstructor() {
            game = new Game(new Player("Test", 5, 5, 5), new Story("Test", new Passage("Test", "Test")), new ArrayList<Goal<?>>());
            assertEquals("Test", game.getStory().getTitle());
            assertTrue(game.getStory().getOpeningPassage().getTitle().equals("Test"));
            assertTrue(game.getStory().getOpeningPassage().getContent().equals("Test"));
        }

        @Test
        void testGameConstructorWithNullStory() {
            try {
                game = new Game(new Player("Test", 5, 5, 5), null, new ArrayList<Goal<?>>());
            } catch (IllegalArgumentException e) {
                assertEquals("Story cannot be null.", e.getMessage());
            }
        }

        @Test
        void testGameConstructorWithNullPlayer() {
            try {
                game = new Game(null, new Story("Test", new Passage("Test", "Test")), new ArrayList<Goal<?>>());
            } catch (IllegalArgumentException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testGameConstructorWithNullGoals() {
            try {
                game = new Game(new Player("Test", 5, 5, 5), new Story("Test", new Passage("Test", "Test")), null);
            } catch (IllegalArgumentException e) {
                assertEquals("Goals cannot be null.", e.getMessage());
            }
        }
    }

    @Nested
    class GameSetters {

        Game game;
        @BeforeEach
        void setUp() {
            game = new Game(new Player("Test", 5, 5, 5), new Story("Test", new Passage("Test", "Test")), new ArrayList<Goal<?>>());
        }

        @Test
        void testSetStory() {
            game.setStory(new Story("Test2", new Passage("Test2", "Test2")));
            assertEquals("Test2", game.getStory().getTitle());
            assertTrue(game.getStory().getOpeningPassage().getTitle().equals("Test2"));
            assertTrue(game.getStory().getOpeningPassage().getContent().equals("Test2"));
        }

        @Test
        void testSetStoryWithNullStory() {
            try {
                game.setStory(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Story cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetPlayer() {
            game.setPlayer(new Player("Test2", 5, 5, 5));
            assertEquals("Test2", game.getPlayer().getName());
        }

        @Test
        void testSetPlayerWithNullPlayer() {
            try {
                game.setPlayer(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetGoals() {
            List<Goal<?>> goals = new ArrayList<Goal<?>>();
            goals.add(new ScoreGoal(10));
            game.setGoals(goals);
            assertTrue(game.getGoals().iterator().next() instanceof ScoreGoal);
        }

        @Test
        void testSetGoalsWithNullGoals() {
            try {
                game.setGoals(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Goals cannot be null.", e.getMessage());
            }
        }
    }

    @Nested
    class GameMethods {

        Game game;

        @BeforeEach
        void setUp() {
            game = new Game(new Player("Test", 5, 5, 5), new Story("Test", new Passage("Test", "Test")), new ArrayList<Goal<?>>());
        }

        @Test
        void testGo() {
            Passage passageOne = new Passage("Test", "Test");
            game.getStory().addPassage(passageOne);
            Passage passage = game.go(new Link("Test", "Test"));
            assertEquals(passageOne.getTitle(), passage.getTitle());
            assertEquals(passageOne.getContent(), passage.getContent());
        }

        @Test
        void testBegin() {
            Passage passage = game.begin();
            assertEquals(game.getStory().getOpeningPassage().getTitle(), passage.getTitle());
            assertEquals(game.getStory().getOpeningPassage().getContent(), passage.getContent());
        }
    }
}
