package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Link;
import edu.ntnu.idatt2001.callumg.model.actions.AddGoldAction;
import edu.ntnu.idatt2001.callumg.model.actions.AddHealthAction;

public class LinkTest {
    
    @Nested
    class LinkConstructor {
        Link link;
        
        @Test
        void testConstructor() {
            link = new Link("Test", "Test");
            assertEquals("Test", link.getText());
            assertEquals("Test", link.getReference());
        }

        @Test
        void testConstructorWithNullText() {
            try {
                link = new Link(null, "Test");
            } catch (IllegalArgumentException e) {
                assertEquals("Text cannot be null.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithNullReference() {
            try {
                link = new Link("Test", null);
            } catch (IllegalArgumentException e) {
                assertEquals("Reference cannot be null.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithBlankText() {
            try {
                link = new Link("", "Test");
            } catch (IllegalArgumentException e) {
                assertEquals("Text cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testConstructorWithBlankReference() {
            try {
                link = new Link("Test", "");
            } catch (IllegalArgumentException e) {
                assertEquals("Reference cannot be blank.", e.getMessage());
            }
        }
    }

    @Nested
    class LinkSetters {
        Link link;

        @BeforeEach
        void setUp() {
            link = new Link("Test", "Test");
        }

        @Test
        void testSetText() {
            link.setText("Test2");
            assertEquals("Test2", link.getText());
        }

        @Test
        void testSetTextWithNull() {
            try {
                link.setText(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Text cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetTextWithBlank() {
            try {
                link.setText("");
            } catch (IllegalArgumentException e) {
                assertEquals("Text cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testSetReference() {
            link.setReference("Test2");
            assertEquals("Test2", link.getReference());
        }

        @Test
        void testSetReferenceWithNull() {
            try {
                link.setReference(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Reference cannot be null.", e.getMessage());
            }
        }

        @Test
        void testSetReferenceWithBlank() {
            try {
                link.setReference("");
            } catch (IllegalArgumentException e) {
                assertEquals("Reference cannot be blank.", e.getMessage());
            }
        }
    }

    @Nested
    class LinkEquals {
        Link link1;
        Link link2;

        @BeforeEach
        void setUp() {
            link1 = new Link("Test", "Test");
            link2 = new Link("Test", "Test");
        }

        @Test
        void testEquals() {
            assertTrue(link1.equals(link2));
        }

        @Test
        void testEqualsWithNull() {
            assertTrue(!link1.equals(null));
        }

        @Test
        void testEqualsWithDifferentClass() {
            assertTrue(!link1.equals("Test"));
        }
        
        @Test
        void testEqualsWithDifferentText() {
            link2.setText("Test2");
            assertTrue(!link1.equals(link2));
        }

        @Test
        void testEqualsWithDifferentReference() {
            link2.setReference("Test2");
            assertTrue(!link1.equals(link2));
        }

        @Test
        void testEqualsWithDifferentTextAndReference() {
            link2.setText("Test2");
            link2.setReference("Test2");
            assertTrue(!link1.equals(link2));
        }

        @Test
        void testEqualsWithDifferentActions() {
            link1.addAction(new AddGoldAction(10));
            link2.addAction(new AddHealthAction(10));
            assertTrue(!link1.equals(link2));
        }

    }

    @Nested
    class LinkActions {
        Link link;

        @BeforeEach
        void setUp() {
            link = new Link("Test", "Test");
        }

        @Test
        void testAddAction() {
            link.addAction(new AddGoldAction(10));
            assertEquals(1, link.getActions().size());
        }

        @Test
        void testAddActionWithNull() {
            try {
                link.addAction(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Action cannot be null.", e.getMessage());
            }
        }

        @Test
        void testRemoveAction() {
            AddGoldAction action = new AddGoldAction(10);
            link.addAction(action);
            link.removeAction(action);
            assertEquals(0, link.getActions().size());
        }

        @Test
        void testRemoveActionWithNull() {
            try {
                link.removeAction(null);
            } catch (IllegalArgumentException e) {
                assertEquals("Action cannot be null.", e.getMessage());
            }
        }

        @Test
        void testRemoveActionWithNonExistentAction() {
            assertFalse(link.removeAction(new AddGoldAction(10)));
        }
    }
}
