package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Game;
import edu.ntnu.idatt2001.callumg.model.Link;
import edu.ntnu.idatt2001.callumg.model.Passage;
import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.Story;
import edu.ntnu.idatt2001.callumg.model.actions.AddGoldAction;
import edu.ntnu.idatt2001.callumg.model.actions.AddInventoryItemAction;
import edu.ntnu.idatt2001.callumg.model.actions.RemoveGoldAction;
import edu.ntnu.idatt2001.callumg.model.goals.Goal;
import edu.ntnu.idatt2001.callumg.model.goals.GoldGoal;
import edu.ntnu.idatt2001.callumg.model.goals.ItemGoal;
import edu.ntnu.idatt2001.callumg.service.GameService;

public class GameServiceTest {
    
    @Nested
    class RegisterGame {
        GameService gameService;
        List<Goal<?>> goals;
        Game game;
        Player player;
        Story story;
        GoldGoal goldGoal;
        ItemGoal itemGoal;
        Passage passage;
        Passage passage2;
        Link link;
        AddGoldAction addGoldAction;
        RemoveGoldAction removeGoldAction;
        AddInventoryItemAction addInventoryItemAction;
        
        @BeforeEach
        void setUp() {  
            try {
                gameService = new GameService();
                gameService.deleteAll();
                goals = new ArrayList<Goal<?>>();
                goldGoal = new GoldGoal(10);
                itemGoal = new ItemGoal("Sword");
                goals.add(goldGoal);
                goals.add(itemGoal);
                passage = new Passage("Room 1", "You are in a room.");
                passage2 = new Passage("Room 2", "You are in another room.");
                player = new Player("Callum", 10, 10, 10);
                story = new Story("Battle", passage);
                link = new Link("An orc appears", "Room 1");
                addGoldAction = new AddGoldAction(10);
                removeGoldAction = new RemoveGoldAction(10);
                addInventoryItemAction = new AddInventoryItemAction("Sword");
                link.addAction(addGoldAction);
                link.addAction(removeGoldAction);
                link.addAction(addInventoryItemAction);
                passage.addLink(link);
                story.addPassage(passage2);
                game = new Game(player, story, goals);
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            }
        }

        @AfterAll
        static void tearDown() {
            try {
                GameService gameService = new GameService();
                gameService.deleteAll();
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            }
        }

        @Test
        void testAddGame() {
            game = gameService.add(game);
            Game gameFromDB = gameService.getById(game.getId()).get();
            assertEquals(game.getId(), gameFromDB.getId());
            assertEquals(game.getGoals().get(0).getVal(), gameFromDB.getGoals().get(0).getVal());
            assertEquals(game.getPlayer().getName(), gameFromDB.getPlayer().getName());
            assertEquals(game.getStory().getTitle(), gameFromDB.getStory().getTitle());
            assertEquals(game.getStory().getOpeningPassage().getTitle(), gameFromDB.getStory().getOpeningPassage().getTitle());
            assertEquals(game.getGoals().get(1).getVal(), gameFromDB.getGoals().get(1).getVal());
            assertEquals(game.getStory().getOpeningPassage().getLinks().get(0).getActions().get(0).getVal(), gameFromDB.getStory().getOpeningPassage().getLinks().get(0).getActions().get(0).getVal());
            assertEquals(game.getStory().getOpeningPassage().getLinks().get(0).getActions().get(1).getVal(), gameFromDB.getStory().getOpeningPassage().getLinks().get(0).getActions().get(1).getVal());
            assertEquals(game.getStory().getOpeningPassage().getLinks().get(0).getActions().get(2).getVal(), gameFromDB.getStory().getOpeningPassage().getLinks().get(0).getActions().get(2).getVal());
            assertEquals(game.getStory().getPassages().size(), gameFromDB.getStory().getPassages().size());
            assertEquals(game.getStory().getPassages().iterator().next().getTitle(), gameFromDB.getStory().getPassages().iterator().next().getTitle());
        }
    }
}
