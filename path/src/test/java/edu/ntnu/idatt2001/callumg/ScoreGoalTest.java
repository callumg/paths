package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddScoreAction;
import edu.ntnu.idatt2001.callumg.model.actions.IntegerAction;
import edu.ntnu.idatt2001.callumg.model.goals.IntegerGoal;
import edu.ntnu.idatt2001.callumg.model.goals.ScoreGoal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ScoreGoalTest {
    
    @Nested
    class ScoreGoalConstructor {

        IntegerGoal goal;

        @Test
        void testScoreGoalConstructorWithNegativeScore() {
            try {
                goal = new ScoreGoal(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class ScoreGoalIsCompleted {

        Player player;
        IntegerGoal goal;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 5, 5, 5);
        }

        @Test
        void testScoreGoalIsCompletedWithNegativeScore() {
            try {
                goal = new ScoreGoal(10);
                goal.isCompleted(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testScoreGoalIsCompletedWithNotEnoughScore() {
            goal = new ScoreGoal(10);
            assertEquals(false, goal.isCompleted(player));
        }

        @Test
        void testScoreGoalIsCompletedWithEnoughScore() {
            goal = new ScoreGoal(10);
            IntegerAction action = new AddScoreAction(10);
            action.execute(player);
            assertEquals(true, goal.isCompleted(player));
        }
    }
}

