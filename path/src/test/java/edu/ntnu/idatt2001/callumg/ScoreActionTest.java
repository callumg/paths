package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddScoreAction;
import edu.ntnu.idatt2001.callumg.model.actions.IntegerAction;
import edu.ntnu.idatt2001.callumg.model.actions.RemoveScoreAction;

public class ScoreActionTest {
    @Nested
    class AddScoreActionTests {

        Player player;
        IntegerAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testAddScoreAction() {
            action = new AddScoreAction(10);
            action.execute(player);
            assertEquals(20, player.getScore());
        }

        @Test
        void testAddScoreActionWithNegativeScore() {
            try {
                action = new AddScoreAction(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class RemoveScoreActionTests {
        Player player;
        IntegerAction action;

        @BeforeEach
        void setUp() {
            player = new Player("Test", 10, 10, 10);
        }

        @Test
        void testRemoveScoreAction() {
            action = new RemoveScoreAction(10);
            action.execute(player);
            assertEquals(0, player.getScore());
        }

        @Test
        void testRemoveScoreActionWithNegativeScore() {
            try {
                action = new RemoveScoreAction(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }
}
