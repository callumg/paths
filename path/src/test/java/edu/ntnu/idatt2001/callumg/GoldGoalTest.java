package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.actions.AddGoldAction;
import edu.ntnu.idatt2001.callumg.model.goals.GoldGoal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class GoldGoalTest {

    GoldGoal goal;
    
    @Nested
    class goldGoalConstructor {
        @Test
        void testGoldGoalConstructorWithNegativeGold() {
            try {
                goal = new GoldGoal(-10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be less than 0.", e.getMessage());
            }
        }
    }

    @Nested
    class goldGoalIsCompleted {
        Player player;
        @BeforeEach
        void setUp() {
            player = new Player("Test", 5, 5, 5);
        }

        @Test
        void testGoldGoalIsCompletedWithNegativeGold() {
            try {
                GoldGoal goal = new GoldGoal(10);
                goal.isCompleted(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testGoldGoalIsCompletedWithNotEnoughGold() {
            GoldGoal goal = new GoldGoal(10);
            assertEquals(false, goal.isCompleted(player));
        }

        @Test
        void testGoldGoalIsCompletedWithEnoughGold() {
            GoldGoal goal = new GoldGoal(10);
            AddGoldAction action = new AddGoldAction(10);
            action.execute(player);
            assertEquals(true, goal.isCompleted(player));
        }
    }

}
