package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.goals.ItemGoal;

public class ItemGoalTest {
    ItemGoal goal;

    @Nested
    class ItemGoalConstructor {
        @Test
        void testItemGoalConstructorWithNullItem() {
            try {
                goal = new ItemGoal("Item");
                assertEquals("Item", goal.getVal());
            } catch (NullPointerException | IllegalArgumentException e) {
                fail();
            }
        }

        @Test
        void testItemGoalIsCompletedWithNullItem() {
            try {
                goal = new ItemGoal(null);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Value cannot be null.", e.getMessage());
            }
        }

        @Test
        void testItemGoalConstructorWithEmptyItem() {
            try {
                goal = new ItemGoal("");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Value cannot be blank.", e.getMessage());
            }
        }
    }

    @Nested
    class ItemGoalIsCompleted {
        Player player;
        ItemGoal goal;

        @BeforeEach
        void setUp() {
            try {
                goal = new ItemGoal("item");
                player = new Player("Test", 10, 10, 10);
            } catch (NullPointerException | IllegalArgumentException e) {
                fail();
            }
        }

        @Test
        void testItemGoalIsCompletedWithNullPlayer() {
            try {
                player = null;
                goal.isCompleted(player);
                fail();
            } catch (NullPointerException e) {
                assertEquals("Player cannot be null.", e.getMessage());
            }
        }

        @Test
        void testItemGoalIsCompletedWithPlayerWithItem() {
            try {
                player.getInventory().add("item");
                assertEquals(true, goal.isCompleted(player));
            } catch (NullPointerException e) {
                fail();
            }
        }

        @Test
        void testItemGoalIsCompletedWithPlayerWithoutItem() {
            try {
                assertEquals(false, goal.isCompleted(player));
            } catch (NullPointerException e) {
                fail();
            }
        }
    }
}
