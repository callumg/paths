package edu.ntnu.idatt2001.callumg.model.goals;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;

/**
 * Class AbstractGoal.
 * An abstract class that represents a goal.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@Inheritance
@DiscriminatorColumn(name = "goal_type")
@Table(name = "goal")
public abstract class AbstractGoal<T> implements Goal<T> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    @Column(name = "goal_id", nullable = false, updatable = false)
    private Long goalId;

    /**
     * Method getGoalId.
     * Gets the goal id.
     */
    public Long getGoalId() {
        return goalId;
    }

    /**
     * Method setGoalId.
     * Sets the goal id.
     * @param goalId The goal id.
     * @throws IllegalArgumentException if the goal id is less than 0.
     */
    public void setGoalId(Long goalId) throws IllegalArgumentException {
        if (goalId < 0) throw new IllegalArgumentException("Goal id cannot be negative.");
        this.goalId = goalId;
    }
}
