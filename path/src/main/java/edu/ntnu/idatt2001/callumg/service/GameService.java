package edu.ntnu.idatt2001.callumg.service;

import edu.ntnu.idatt2001.callumg.model.Game;
import edu.ntnu.idatt2001.callumg.repository.GameRepository;

/**
 * Class GameService
 * @author Callum G.
 * @version 1.1 5.2.2023
 */
public class GameService extends AbstractService<Game, Long> {

    /**
     * Constructor for GameService.
     * @throws NullPointerException if the entity manager or repository is null.
     */
    public GameService() throws NullPointerException {
        super(new GameRepository());
    }
}
