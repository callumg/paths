package edu.ntnu.idatt2001.callumg.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Class User.
 * @author Callum G.
 * @version 1.0 5.2.2023
 */
@Entity
@Table(name = "user")
public class User {
    
    @Id
    @Column(name = "username", length = 64, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String hashedPassword;

    @Column(name = "salt", nullable = false)
    private byte[] salt;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="username", referencedColumnName = "username")
    private List<Game> games;

    /**
     * Constructor for User.
     * @param username The username of the user.
     * @param hash The hash of the user.
     * @param salt The salt of the user.
     * @throws NullPointerException if the username, hash or salt is null.
     * @throws IllegalArgumentException if the username or hash is empty.
     */
    public User(String username, String hashedPassword, byte[] salt) throws NullPointerException {
        Objects.requireNonNull(username, "The username cannot be null.");
        Objects.requireNonNull(hashedPassword, "The hashed password cannot be null.");
        Objects.requireNonNull(salt, "The salt cannot be null.");
        if (username.isBlank()) throw new IllegalArgumentException("The username cannot be empty.");
        if (hashedPassword.isBlank()) throw new IllegalArgumentException("The hashed password cannot be empty.");
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.games = new ArrayList<Game>();
    }

    /**
     * Empty constructor for User.
     */
    public User() {}

    /**
     * Gets the username.
     * @return Returns the username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     * @param username The username to set.
     * @throws NullPointerException if the username is null.
     * @throws IllegalArgumentException if the username is empty.
     */
    public void setUsername(String username) throws NullPointerException {
        Objects.requireNonNull(username, "The username cannot be null.");
        if (username.isBlank()) throw new IllegalArgumentException("The username cannot be empty.");
        this.username = username;
    }

    /**
     * Gets the hashed password.
     * @return Returns the hashed password.
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * Sets the hashed password.
     * @param hashedPassword The hashed password to set.
     * @throws NullPointerException if the hashed password is null.
     * @throws IllegalArgumentException if the hashed password is empty.
     */
    public void setHashedPassword(String hashedPassword) throws NullPointerException {
        Objects.requireNonNull(hashedPassword, "The hashed password cannot be null.");
        if (hashedPassword.isBlank()) throw new IllegalArgumentException("The hashed password cannot be empty.");
        this.hashedPassword = hashedPassword;
    }

    /**
     * Gets the salt.
     * @return Returns the salt.
     */
    public byte[] getSalt() {
        return salt;
    }

    /**
     * Sets the salt.
     * @param salt The salt to set.
     * @throws NullPointerException if the salt is null.
     */
    public void setSalt(byte[] salt) throws NullPointerException {
        Objects.requireNonNull(salt, "The salt cannot be null.");
        this.salt = salt;
    }

    /**
     * Gets the games.
     * @return Returns the games.
     */
    public List<Game> getGames() {
        return games;
    }

    /**
     * Sets the games.
     * @param games The games to set.
     * @throws NullPointerException if the games is null.
     */
    public void setGames(List<Game> games) throws NullPointerException {
        Objects.requireNonNull(games, "The games cannot be null.");
        this.games = games;
    }

    /**
     * Method addGame.
     * Adds a game to the list of games.
     * @param game The game to add.
     * @throws NullPointerException if the game is null.
     */
    public void addGame(Game game) throws NullPointerException {
        Objects.requireNonNull(game, "The game cannot be null.");
        System.out.println("Added game");
        games.add(game);
    }

    /**
     * Method removeGame.
     * Removes a game from the list of games.
     * @param game The game to remove.
     * @throws NullPointerException if the game is null.
     */
    public void removeGame(Game game) throws NullPointerException {
        Objects.requireNonNull(game, "The game cannot be null.");
        games.remove(game);
    }
}
