package edu.ntnu.idatt2001.callumg.model;

import java.lang.IllegalArgumentException;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.ArrayList;

/**
 * Class Passage.
 * A class that represents a passage in the game.
 * @author Callum G.
 * @version 1.0 1.2.2022
 */
@Entity
@Table (name = "passage")
public class Passage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "passage_id", nullable = false, updatable = false)
    private Long id;
    
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "passage_id", referencedColumnName = "passage_id")
    private List<Link> links;

    /**
     * Constructor for Passage.
     * @param title The title of the passage.
     * @param content The content of the passage.
     * @throws IllegalArgumentException if the title or content is blank or null.
     */
    public Passage(String title, String content) throws IllegalArgumentException {
        if (title == null) throw new IllegalArgumentException("Title cannot be null.");
        if (content == null) throw new IllegalArgumentException("Content cannot be null.");
        if (title.isBlank()) throw new IllegalArgumentException("Title cannot be blank.");
        if (content.isBlank()) throw new IllegalArgumentException("Content cannot be blank.");
        this.title = title;
        this.content = content;
        this.links = new ArrayList<Link>();
    }

    /**
     * Empty constructor for Passage.
     */
    public Passage() {}

    /**
     * Method getId.
     * @return The id of the passage.
     */
    public Long getId() {
        return this.id;
    }

    /**
     *  Method getTitle.
     *  @return The title of the passage.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     *  Method getContent.
     *  @return The content of the passage.
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Method addLink.
     * @param link The link that will be added to the list of links.
     * @throws IllegalArgumentException if the link is null.
     */
    public boolean addLink(Link link) throws IllegalArgumentException {
        if (link == null) throw new IllegalArgumentException("Link cannot be null.");
        return this.links.add(link);
    }

    /**
     * Method getLinks.
     * @return The list of links.
     */
    public List<Link> getLinks() {
        return this.links;
    }

    /**
     * Predicate method hasLinks.
     * @return True if the list of links is not empty, false otherwise.
     */
    boolean hasLinks() {
        return !this.links.isEmpty();
    }

    /**
     * Method setLinks.
     * @param links The list of links that will replace the current list of links.
     * @throws IllegalArgumentException if the list of links is null.
     */
    public void setLinks(List<Link> links) throws IllegalArgumentException {
        if (links == null) throw new IllegalArgumentException("Links cannot be null.");
        this.links = links;
    }

    /**
     * Method setId.
     * @param id The id of the passage.
     * @throws IllegalArgumentException if the id is less than 0.
     */
    public void setId(Long id) throws IllegalArgumentException {
        if (id < 0) throw new IllegalArgumentException("Id cannot be less than 0.");
        this.id = id;
    }

    /**
     * Method setTitle.
     * @param title The title of the passage.
     * @throws IllegalArgumentException if the title is blank or null.
     */
    public void setTitle(String title) throws IllegalArgumentException {
        if (title == null) throw new IllegalArgumentException("Title cannot be null.");
        if (title.isBlank()) throw new IllegalArgumentException("Title cannot be blank.");
        this.title = title;
    }

    /**
     * Method setContent.
     * @param content The content of the passage.
     * @throws IllegalArgumentException if the content is blank or null.
     */
    public void setContent(String content) throws IllegalArgumentException {
        if (content == null) throw new IllegalArgumentException("Content cannot be null.");
        if (content.isBlank()) throw new IllegalArgumentException("Content cannot be blank.");
        this.content = content;
    }

    /**
     * Method toString.
     * @return A string representation of the passage.
     */
    @Override
    public String toString() {
        return "Passage{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", links=" + links +
                '}';
    }

    /**
     * Method equals.
     * @param o The object that will be compared to the passage.
     * @return True if the object is equal to the passage, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passage passage = (Passage) o;

        if (!title.equals(passage.title)) return false;
        if (!content.equals(passage.content)) return false;
        return links.equals(passage.links);
    }

    /**
     * Method hashCode.
     * @return The hash code of the passage.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.title, this.content, this.links);
    }
    
}
