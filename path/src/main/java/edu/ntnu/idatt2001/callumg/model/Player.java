package edu.ntnu.idatt2001.callumg.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import edu.ntnu.idatt2001.callumg.utils.StringListConverter;


/** Class Story.
 * A class that represents a player in the game.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@Table (name = "player")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 64)
    private String name;

    @Column(name = "health", nullable = false)
    private int health;

    @Column(name = "score", nullable = false)
    private int score;

    @Column(name = "gold", nullable = false)
    private int gold;

    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name = "inventory", nullable = false)
    @Convert(converter = StringListConverter.class)
    private List<String> inventory;

    /**
     * Constructor for Player.
     * @param name The name of the player.
     * @param health The health of the player.
     * @param score The score of the player.
     * @param gold The gold of the player.
     * @param inventory The inventory of the player.
     * @throws IllegalArgumentException if the name is blank or null.
     * @throws IllegalArgumentException if the health is negative.
     * @throws IllegalArgumentException if the score is negative.
     * @throws IllegalArgumentException if the gold is negative.
     */
    public Player(String name, int health, int score, int gold, List<String> inventory) throws IllegalArgumentException {
        if (name == null) throw new IllegalArgumentException("Name cannot be null.");
        if (name.isBlank()) throw new IllegalArgumentException("Name cannot be blank.");
        if (health < 0) throw new IllegalArgumentException("Health cannot be negative.");
        if (score < 0) throw new IllegalArgumentException("Score cannot be negative.");
        if (gold < 0) throw new IllegalArgumentException("Gold cannot be negative.");
        if (inventory == null) throw new IllegalArgumentException("Inventory cannot be null.");
        this.name = name;
        this.health = health;
        this.score = score;
        this.gold = gold;
        this.inventory = inventory;
    }

    /**
     * Constructor for Player.
     * @param name The name of the player.
     * @param health The health of the player.
     * @param score The score of the player.
     * @param gold The gold of the player.
     * @throws IllegalArgumentException if the name is blank.
     * @throws IllegalArgumentException if the health is negative.
     * @throws IllegalArgumentException if the score is negative.
     * @throws IllegalArgumentException if the gold is negative.
     */
    public Player(String name, int health, int score, int gold) throws IllegalArgumentException {
        this(name, health, score, gold, new ArrayList<String>());
    }

    /**
     * Constructor for Player.
     * @param player The player to copy.
     */
    public Player(Player player) {
        this(player.getName(), player.getHealth(), player.getScore(), player.getGold(), player.getInventory());
    }

    /**
     * Default constructor for JPA.
     */
    public Player() {}

    /**
     * Method getId.
     * @return The id of the player.
     */
    public Long getId() {
        return id;
    }

    /**
     * Method getName.
     * @return The name of the player.
     */
    public String getName() {
        return name;
    }

    /**
     * Method getHealth.
     * @return The health of the player.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Method getScore.
     * @return The score of the player.
     */
    public int getScore() {
        return score;
    }

    /**
     * Method getGold.
     * @return The gold of the player.
     */
    public int getGold() {
        return gold;
    }

    /**
     * Method getInventory.
     * @return The inventory of the player.
     */
    public List<String> getInventory() {
        return inventory;
    }

    /**
     * Method setId.
     * @param id The id of the player.
     * @throws IllegalArgumentException if the id is negative.
     */
    public void setId(Long id) throws IllegalArgumentException {
        if (id < 0) throw new IllegalArgumentException("Id cannot be negative.");
        this.id = id;
    }

    /**
     * Method setName.
     * @param name The name of the player.
     * @throws IllegalArgumentException if the name is blank or null.
     */
    public void setName(String name) throws IllegalArgumentException {
        if (name == null) throw new IllegalArgumentException("Name cannot be null.");
        if (name.isBlank()) throw new IllegalArgumentException("Name cannot be blank.");
        this.name = name;
    }

    /**
     * Method setHealth.
     * @param health The health of the player.
     * @throws IllegalArgumentException if the health is negative.
     */
    public void setHealth(int health) throws IllegalArgumentException {
        if (health < 0) throw new IllegalArgumentException("Health cannot be negative.");
        this.health = health;
    }

    /**
     * Method setScore.
     * @param score The score of the player.
     * @throws IllegalArgumentException if the score is negative.
     */
    public void setScore(int score) throws IllegalArgumentException {
        if (score < 0) throw new IllegalArgumentException("Score cannot be negative.");
        this.score = score;
    }

    /**
     * Method setGold.
     * @param gold The gold of the player.
     * @throws IllegalArgumentException if the gold is negative.
     */
    public void setGold(int gold) throws IllegalArgumentException {
        if (gold < 0) throw new IllegalArgumentException("Gold cannot be negative.");
        this.gold = gold;
    }

    /**
     * Method setInventory.
     * @param inventory The inventory of the player.
     * @throws IllegalArgumentException if the inventory is null.
     */
    public void setInventory(List<String> inventory) throws IllegalArgumentException {
        if (inventory == null) throw new IllegalArgumentException("Inventory cannot be null.");
        this.inventory = inventory;
    }

    /**
     * Method addToInventory.
     * @param item The item to add to the inventory.
     * @throws IllegalArgumentException if the item is blank or null.
     */
    public boolean addToInventory(String item) throws IllegalArgumentException {
        if (item == null) throw new IllegalArgumentException("Item cannot be null.");
        if (item.isBlank()) throw new IllegalArgumentException("Item cannot be blank.");
        return inventory.add(item);
    }

    /**
     * Method removeFromInventory.
     * @param item The item to remove from the inventory.
     * @throws IllegalArgumentException if the item is blank or null.
     */
    public boolean removeFromInventory(String item) throws IllegalArgumentException {
        if (item == null) throw new IllegalArgumentException("Item cannot be null.");
        if (item.isBlank()) throw new IllegalArgumentException("Item cannot be blank.");
        return inventory.remove(item);
    }

    /**
     * Method addHealth.
     * @param health The health to add to the player.
     * @throws IllegalArgumentException if the health is negative.
     */
    public void addHealth(int health) {
        if (health < 0) throw new IllegalArgumentException("Health cannot be negative.");
        setHealth(health + getHealth());
    }

    /**
     * Method addScore.
     * @param score The score to add to the player.
     * @throws IllegalArgumentException if the score is negative.
     */
    public void addScore(int score) {
        if (score < 0) throw new IllegalArgumentException("Score cannot be negative.");
        setScore(score + getScore());
    }

    /**
     * Method addGold.
     * @param gold The gold to add to the player.
     * @throws IllegalArgumentException if the gold is negative.
     */
    public void addGold(int gold) {
        if (gold < 0) throw new IllegalArgumentException("Gold cannot be negative.");
        setGold(gold + getGold());
    }

    /**
     * Method removeGold.
     * @param gold The gold to remove from the player.
     * @throws IllegalArgumentException if the gold is negative.
     */
    public void removeGold(int gold) {
        if (gold < 0) throw new IllegalArgumentException("Gold cannot be negative.");
        setGold(getGold() - gold);
    }

    /**
     * Method removeScore.
     * @param score The score to remove from the player.
     * @throws IllegalArgumentException if the score is negative.
     */
    public void removeScore(int score) {
        if (score < 0) throw new IllegalArgumentException("Score cannot be negative.");
        setScore(getScore() - score);
    }

    /**
     * Method removeHealth.
     * @param health The health to remove from the player.
     * @throws IllegalArgumentException if the health is negative.
     */
    public void removeHealth(int health) {
        if (health < 0) throw new IllegalArgumentException("Health cannot be negative.");
        setHealth(getHealth() - health);
    }

    /**
     * Method toString.
     * @return A string representation of the player.
     */
    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", score=" + score +
                ", gold=" + gold +
                ", inventory=" + inventory +
                '}';
    }

    /**
     * Method equals.
     * @param o The object to compare to.
     * @return True if the objects are equal, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (health != player.health) return false;
        if (score != player.score) return false;
        if (gold != player.gold) return false;
        if (!name.equals(player.name)) return false;
        return inventory.equals(player.inventory);
    }

    /**
     * Method hashCode.
     * @return The hash code of the player.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, health, score, gold, inventory);
    }
}
