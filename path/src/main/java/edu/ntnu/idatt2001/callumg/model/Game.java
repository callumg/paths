package edu.ntnu.idatt2001.callumg.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import edu.ntnu.idatt2001.callumg.model.goals.AbstractGoal;
import edu.ntnu.idatt2001.callumg.model.goals.Goal;

import javax.persistence.CascadeType;

/**
 * Class Game.
 * A class that represents a game.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */

@Entity
@Table(name = "game")
public class Game {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    @Column(name = "game_id", unique = true, nullable = false)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_id", referencedColumnName = "player_id")
    private Player player;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "story_id", referencedColumnName = "story_id")
    private Story story;

    @ManyToMany(cascade = CascadeType.ALL, targetEntity=AbstractGoal.class)
    @JoinTable(
            name = "game_goal",
            joinColumns = {@JoinColumn(name = "game_id", referencedColumnName = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "goal_id", referencedColumnName = "goal_id")}
    )
    private List<Goal<?>> goals;

    /**
     * Constructor for Game.
     * @param player The player to play the game.
     * @param story The story to play.
     * @param goals The goals to complete.
     * @throws IllegalArgumentException if the player is null, the story is null, or the goals is null.
     */
    public Game(Player player, Story story, List<Goal<?>> goals) throws IllegalArgumentException {
        if (player == null) throw new IllegalArgumentException("Player cannot be null.");
        if (story == null) throw new IllegalArgumentException("Story cannot be null.");
        if (goals == null) throw new IllegalArgumentException("Goals cannot be null.");
        this.player = player;
        this.story = story;
        this.goals = goals;
    }

    /**
     * Empty constructor for Game.
     */
    public Game() {}

    /**
     * Method getPlayer.
     * @return The player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method getStory.
     * @return The story.
     */
    public Story getStory() {
        return story;
    }

    /**
     * Method getGoals.
     * @return The goals.
     */
    public List<Goal<?>> getGoals() {
        return goals;
    }

    /**
     * Method begin.
     * @return The opening passage of the story.
     */
    public Passage begin() {
        return story.getOpeningPassage();
    }
    
    /**
     * Method getPassage.
     * @param link The link to the passage.
     * @return The passage.
     */
    public Passage go(Link link) {
        return story.getPassage(link);
    }

    /**
     * Method getId.
     * @return The id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Method setId.
     * @param id The id to set.
     * @throws IllegalArgumentException if the id is less than 0.
     */
    public void setId(Long id) throws IllegalArgumentException {
        if (id < 0) throw new IllegalArgumentException("Id cannot be less than 0.");
        this.id = id;
    }
    
    /**
     * Method setGoals.
     * @param goals The goals to set.
     * @throws IllegalArgumentException if the goals is null.
     */
    public void setGoals(List<Goal<?>> goals) throws IllegalArgumentException {
        if (goals == null) throw new IllegalArgumentException("Goals cannot be null.");
        this.goals = goals;
    }

    /**
     * Method setPlayer.
     * @param player The player to set.
     * @throws IllegalArgumentException if the player is null.
     */
    public void setPlayer(Player player) throws IllegalArgumentException {
        if (player == null) throw new IllegalArgumentException("Player cannot be null.");
        this.player = player;
    }

    /**
     * Method setStory.
     * @param story The story to set.
     * @throws IllegalArgumentException if the story is null.
     */
    public void setStory(Story story) throws IllegalArgumentException {
        if (story == null) throw new IllegalArgumentException("Story cannot be null.");
        this.story = story;
    }
}
