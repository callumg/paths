package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class AddInventoryItemAction.
 * A class that represents an action that adds an inventory item to the player's inventory.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("add_item")
public class AddInventoryItemAction extends StringAction {

    /**
     * Constructor for AddInventoryItemAction.
     * @param inventoryItem The inventory item that will be added to the player's inventory.
     * @throws NullPointerException if the inventory item is null.
     * @throws IllegalArgumentException if the inventory item is empty.
     */
    public AddInventoryItemAction(String inventoryItem) throws NullPointerException, IllegalArgumentException {
        super(inventoryItem);
    }

    /**
     * Empty constructor for JPA.
     */
    public AddInventoryItemAction() {}

    /**
     * Method execute.
     * @param player The player that will have the inventory item added to their inventory.
     * @throws IllegalArgumentException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.addToInventory(this.getVal());
    }
}
