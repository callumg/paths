package edu.ntnu.idatt2001.callumg.repository;

import java.util.List;
import java.util.Optional;

import org.hibernate.TransactionException;

/**
 * Interface Repository.
 * A generic interface for a repository.
 * @author Callum G.
 * @version 1.1 5.2.2022
 * @param <T> The type of object to store.
 * @param <ID> The type of the ID of the object.
 */
public interface Repository<T, ID> {
    /**
     * Method to add an object to the repository.
     * @param t The object to add.
     * @return The object added.
     * @throws NullPointerException if the object is null.
     * @throws IllegalArgumentException if the object already exists in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T add(T t) throws NullPointerException, IllegalArgumentException, TransactionException;

    /**
     * Method to remove an object from the repository.
     * @param t The object to remove.
     * @return The object removed.
     * @throws NullPointerException if the object is null.
     * @throws IllegalArgumentException if the object does not exist in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T remove(T t) throws NullPointerException, IllegalArgumentException, TransactionException;

    /**
     * Method to remove an object from the repository.
     * @param id The ID of the object to remove.
     * @return The object removed.
     * @throws NullPointerException if the ID is null.
     * @throws IllegalArgumentException if the object does not exist in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T removeId(ID id) throws NullPointerException, IllegalArgumentException, TransactionException;

    /**
     * Method to update an object in the repository.
     * @param t The object to update.
     * @return The object updated.
     * @throws NullPointerException if the object is null.
     * @throws IllegalArgumentException if the object does not exist in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T update(T t) throws NullPointerException, IllegalArgumentException, TransactionException;

    /**
     * Method to find an object in the repository.
     * @param id The ID of the object to find.
     * @return The object found.
     * @throws NullPointerException if the ID is null.
     */
    public Optional<T> find(ID id) throws NullPointerException;

    /**
     * Method to get all objects in the repository.
     * @return A list of all objects in the repository.
     */
    public List<T> findAll();

    /**
     * Method to get the size of the repository.
     * @return The size of the repository.
     */
    public int size();

    /**
     * Method to check if the repository is empty.
     * @return True if the repository is empty, false otherwise.
     */
    public boolean isEmpty();

    /**
     * Method to clear the repository.
     * @throws TransactionException if the transaction fails.
     */
    public void clear() throws TransactionException;
}
