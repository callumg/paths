package edu.ntnu.idatt2001.callumg.repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.hibernate.TransactionException;

import edu.ntnu.idatt2001.callumg.utils.EntityManagerSingleton;

/**
 * Class AbstractRepository
 * @author Callum G.
 * @version 1.1 5.2.2023
 * @param <T> The type of the entity
 * @param <ID> The type of the ID of the entity
 */
public abstract class AbstractRepository<T, ID> implements Repository<T, ID> {
    private final Class<T> entityClass;

    /**
     * Constructor for AbstractRepository.
     * @param entityManager The entity manager to use.
     * @param entityClass The class of the entity.
     * @throws NullPointerException if the entity manager or entity class is null.
     */
    public AbstractRepository(Class<T> entityClass) throws NullPointerException {
        Objects.requireNonNull(entityClass, "The entity class cannot be null.");
        this.entityClass = entityClass;
    }

    @Override
    public T add(T t) throws NullPointerException, IllegalArgumentException, TransactionException {
        Objects.requireNonNull(t, "The object to add cannot be null.");
        EntityManager entityManager = EntityManagerSingleton.getEntityManager();
        EntityTransaction et = null;
        try {
            et = entityManager.getTransaction();
            et.begin();
            entityManager.persist(t);
            et.commit();
        } catch (IllegalArgumentException | TransactionException e) {
            if (et != null && et.isActive())
                et.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return t;
    }

    @Override
    public T removeId(ID id) throws NullPointerException, IllegalArgumentException, TransactionException {
        Objects.requireNonNull(id, "The id of the object to remove cannot be null.");
        T t = find(id).orElseThrow();
        EntityManager entityManager = EntityManagerSingleton.getEntityManager();
        EntityTransaction et = null;
        try {
            et = entityManager.getTransaction();
            et.begin();
            entityManager.remove(entityManager.contains(t) ? t : entityManager.merge(t));
            et.commit();
        } catch (IllegalArgumentException | TransactionException e) {
            if (et != null && et.isActive())
                et.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return t;
    }

    @Override
    public T remove(T t) throws NullPointerException, IllegalArgumentException, TransactionException {
        Objects.requireNonNull(t, "The object to remove cannot be null.");
        EntityTransaction et = null;
        EntityManager entityManager = EntityManagerSingleton.getEntityManager();
        try {
            et = entityManager.getTransaction();
            et.begin();
            entityManager.remove(entityManager.contains(t) ? t : entityManager.merge(t));
            et.commit();
        } catch (IllegalArgumentException | TransactionException e) {
            if (et != null && et.isActive())
                et.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return t;
    }

    @Override
    public T update(T t) throws NullPointerException, IllegalArgumentException, TransactionException {
        Objects.requireNonNull(t, "The object to add cannot be null.");
        EntityTransaction et = null;
        EntityManager entityManager = EntityManagerSingleton.getEntityManager();
        try {
            et = entityManager.getTransaction();
            et.begin();
            entityManager.merge(t);
            et.commit();
        } catch (IllegalArgumentException | TransactionException e) {
            if (et != null && et.isActive())
                et.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return t;
    }

    @Override
    public Optional<T> find(ID id) throws NullPointerException {
        Objects.requireNonNull(id, "The object to add cannot be null.");
        EntityManager entityManager = EntityManagerSingleton.getEntityManager();
        return Optional.ofNullable(entityManager.find(entityClass, id));
    }

    @Override
    public List<T> findAll() {
        EntityManager entityManager = EntityManagerSingleton.getEntityManager();
        List<T> list = entityManager.createQuery("FROM " + entityClass.getName(), entityClass).getResultList();
        entityManager.close();
        return list;
    }

    @Override
    public int size() {
        return findAll().size();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public void clear() throws NullPointerException, IllegalArgumentException, TransactionException {
        findAll().forEach(this::remove);
    }
}