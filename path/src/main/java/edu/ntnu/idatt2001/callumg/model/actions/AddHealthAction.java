package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class AddHealthAction.
 * A class that represents an action that increases the player's health.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("add_health")
public class AddHealthAction extends IntegerAction {
    
    /**
     * Constructor for AddHealthAction.
     * @param health The health that will be added to the player's health.
     * @throws IllegalArgumentException if the health is less than 0.
     */
    public AddHealthAction(int health) throws IllegalArgumentException {
        super(health);
    }

    /**
     * Empty constructor for JPA.
     */
    public AddHealthAction() {}

    /**
     * Method execute.
     * @param player The player that will have their health increased.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.addHealth(this.getVal());
    }
}
