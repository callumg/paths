package edu.ntnu.idatt2001.callumg.model.goals;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class StringGoal extends AbstractGoal<String> {
    @Column(name = "str_val")
    private String val;
    /**
    * Constructor for StringGoal.
    * @param goal The goal that will be checked against the player's health.
    * @throws NullPointerException if the goal is null.
    * @throws IllegalArgumentException if the goal is less than 0.
    */
    public StringGoal(String val) throws NullPointerException, IllegalArgumentException {
        Objects.requireNonNull(val, "Value cannot be null.");
        if (val.isBlank()) throw new IllegalArgumentException("Value cannot be blank.");
        this.val = val;
    }

    /**
    * Empty constructor for JPA.
    */
    public StringGoal() {}

    /**
    * Method getVal.
    */
    @Override
    public String getVal() {
        return val;
    }

    /**
    * Method setVal.
    * @param val The value that will be set.
    * @throws IllegalArgumentException if the value is less than 0.
    * @throws NullPointerException if the value is null.
    */
    public void setVal(String val) throws IllegalArgumentException {
        Objects.requireNonNull(val, "Value cannot be null.");
        if (val.isBlank()) throw new IllegalArgumentException("Value cannot be blank.");
        this.val = val;
    }
}
