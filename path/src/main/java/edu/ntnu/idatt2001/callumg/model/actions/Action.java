package edu.ntnu.idatt2001.callumg.model.actions;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Interface Action.
 * An interface that represents an action.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
public interface Action<T>{

    /**
     * Method execute.
     * @param player The player that will have the action executed on them.
     * @throws NullPointerException if the player is null.
     */
    public void execute(Player player) throws NullPointerException; // interface method (does not have a body)

    /**
     * Method getId.
     * @return The id of the action.
     */
    public Long getId(); // interface method (does not have a body)

    /**
     * Method getVal.
     * @return The value of the action.
     */
    public T getVal(); // interface method (does not have a body)
}
