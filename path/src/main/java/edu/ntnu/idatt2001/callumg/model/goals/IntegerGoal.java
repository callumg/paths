package edu.ntnu.idatt2001.callumg.model.goals;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class IntegerGoal extends AbstractGoal<Integer> {

    @Column(name = "num_val")
    private int val;

    /**
     * Constructor for IntegerGoal.
     * @param val The goal that will be checked against the player's health.
     * @throws IllegalArgumentException if the goal is less than 0.
     */
    public IntegerGoal(int val) throws IllegalArgumentException {
        if (val < 0) throw new IllegalArgumentException("Value cannot be less than 0.");
        this.val = val;
    }

    /**
     * Empty constructor for JPA.
     */
    public IntegerGoal() {}

    /**
     * Method getVal.
    */
    @Override
    public Integer getVal() {
        return val;
    }

    /**
     * Method setVal.
     * @param val The value that will be set.
     * @throws IllegalArgumentException if the value is less than 0.
     */
    public void setVal(int val) throws IllegalArgumentException {
        if (val < 0) throw new IllegalArgumentException("Value cannot be less than 0.");
        this.val = val;
    }
}
