package edu.ntnu.idatt2001.callumg.utils;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Class StringListConverter.
 * @author Callum G.
 * @version 1.0 4.2.2023
 */
@Converter
public class StringListConverter implements AttributeConverter<List<String>, String> {
    private static final String SPLIT = ",";
    
    /**
     * Converts a list of strings to a string.
     * @param stringList The list of strings.
     * @return Returns the list as a string.
     */
    @Override
    public String convertToDatabaseColumn(List<String> stringList) {
        return stringList != null ? String.join(SPLIT, stringList) : "";
    }

    /**
     * Converts a string to a list of strings.
     * @param string The string.
     * @return Returns the string as a list of strings.
     */
    @Override
    public List<String> convertToEntityAttribute(String string) {
        return string != null ? new ArrayList<>(Arrays.asList(string.split(SPLIT))) : null;
    }
}