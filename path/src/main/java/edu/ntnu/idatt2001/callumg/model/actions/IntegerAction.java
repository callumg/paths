package edu.ntnu.idatt2001.callumg.model.actions;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class IntegerAction extends AbstractAction<Integer> {
    
    @Column(name = "num_val")
    private int val;

    /**
     * Constructor for IntegerAction.
     * @param val The value that will be set.
     * @throws IllegalArgumentException if the value is less than 0.
     */
    public IntegerAction(int val) throws IllegalArgumentException {
        if (val < 0) throw new IllegalArgumentException("Value cannot be less than 0.");
        this.val = val;
    }

    /**
     * Empty constructor for JPA.
     */
    public IntegerAction() {}

    /**
     * Method getVal.
     */
    @Override
    public Integer getVal() {
        return val;
    }

    /**
     * Method setVal.
     * @param val The value that will be set.
     * @throws IllegalArgumentException if the value is less than 0.
     */
    public void setVal(int val) throws IllegalArgumentException {
        if (val < 0) throw new IllegalArgumentException("Value cannot be less than 0.");
        this.val = val;
    }
}
