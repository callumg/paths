package edu.ntnu.idatt2001.callumg.model.goals;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class HealthGoal.
 * A class that represents a health goal.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("health")
public class HealthGoal extends IntegerGoal {

    /**
     * Constructor for HealthGoal.
     * @param goal The goal that will be checked against the player's health.
     * @throws IllegalArgumentException if the goal is less than 0.
     */
    public HealthGoal(int goal) throws NullPointerException, IllegalArgumentException {
        super(goal);
    }

    /**
     * Empty constructor for JPA.
     */
    public HealthGoal() {}

    /**
     * Method isCompleted.
     * @param player The player that will have their health checked.
     * @return true if the player's health is greater than or equal to the goal's health.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public boolean isCompleted(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "Player cannot be null.");
        return player.getHealth() >= this.getVal();
    }
}
