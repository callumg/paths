package edu.ntnu.idatt2001.callumg.service;

import edu.ntnu.idatt2001.callumg.model.User;

/**
 * Class UserStateService.
 * Class that keeps track of the current user.
 * @author Callum G.
 * @version 1.2 14.2.2023
 */
public class UserStateService {
    private static final String GUEST_USER = "guest";

    private static User currentUser;

    private static UserService userService;

    // Static block to initialize the user repository.
    static {
        try {
            userService = new UserService();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Method to get the current user.
     * @return The current user or the guest user if no current user is set.
     */
    public static User getCurrentUser() throws NullPointerException {
        if (currentUser == null) 
            setCurrentUser(userService.getById(GUEST_USER).orElseThrow(() -> new NullPointerException("Guest user does not exist.")));
        return currentUser;
    }

    /**
     * Method to set the current user.
     * @param currentUser The current user.
     */
    public static void setCurrentUser(User currentUser) {
        UserStateService.currentUser = currentUser;
    }
}
