package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class RemoveGoldAction.
 * A class that represents an action that decreases the player's gold.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("remove_gold")
public class RemoveGoldAction extends IntegerAction {
    
    /**
     * Constructor for RemoveGoldAction.
     * @param gold The gold that will be removed from the player's gold.
     * @throws IllegalArgumentException if the gold is less than 0.
     */
    public RemoveGoldAction(int gold) throws IllegalArgumentException {
        super(gold);
    }

    /**
     * Empty constructor for JPA.
     */
    public RemoveGoldAction() {}

    /**
     * Method execute.
     * @param player The player that will have their gold decreased.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.removeGold(this.getVal());
    }
}
