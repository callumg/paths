package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class StringAction extends AbstractAction<String> {
    
    @Column(name = "str_val")
    private String val;

    /**
     * Constructor for StringAction.
     * @param val The value that will be set.
     * @throws NullPointerException if the value is null.
     * @throws IllegalArgumentException if the value is less than 0.
     */
    public StringAction(String val) throws NullPointerException, IllegalArgumentException {
        Objects.requireNonNull(val, "Value cannot be null.");
        if (val.isBlank()) throw new IllegalArgumentException("Value cannot be blank.");
        this.val = val;
    }

    /**
     * Empty constructor for JPA.
     */
    public StringAction() {}

    /**
     * Method getVal.
     */
    @Override
    public String getVal() {
        return val;
    }

    /**
     * Method setVal.
     * @param val The value that will be set.
     * @throws IllegalArgumentException if the value is less than 0.
     * @throws NullPointerException if the value is null.
     */
    public void setVal(String val) throws IllegalArgumentException {
        Objects.requireNonNull(val, "Value cannot be null.");
        if (val.isBlank()) throw new IllegalArgumentException("Value cannot be blank.");
        this.val = val;
    }
}
