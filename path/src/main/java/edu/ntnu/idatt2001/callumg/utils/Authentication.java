package edu.ntnu.idatt2001.callumg.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Objects;

/**
 * Class Authentication.
 * The class that handles the authentication of users.
 * @author Callum G.
 * @version 1.0 5.2.2023
 */
public class Authentication {

    /**
     * Method to generate a salt
     * @return the salt
     * @throws NoSuchAlgorithmException if the algorithm is not found.
     */
    public static byte[] generateSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = new SecureRandom();

        byte[] salt = new byte[16];

        sr.nextBytes(salt);

        return salt;
    }

    /**
     * Method to generate a hashed password
     * @param password the password to hash
     * @param salt the salt to use
     * @return the hashed password
     * @throws NullPointerException if the password or salt is null.
     * @throws IllegalArgumentException if the password is blank.
     * @throws NoSuchAlgorithmException if the algorithm is not found.
     */
    public static String generateHashedPassword(String password, byte[] salt) throws NoSuchAlgorithmException, NullPointerException, IllegalArgumentException {
        Objects.requireNonNull(password, "Password cannot be null.");
        Objects.requireNonNull(salt, "Salt cannot be null.");
        if (password.isBlank()) throw new IllegalArgumentException("Password cannot be blank.");
        
        String hashedPassword = null;

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt);
        byte[] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < bytes.length; i++)
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));

        hashedPassword = sb.toString();

        return hashedPassword;
    }

    /**
     * Method to check if the password is correct.
     * @param password the password to check
     * @param salt the salt to use
     * @param hash the hash to check against
     * @return true if the password is correct, false if not.
     * @throws NullPointerException if the password, salt or hash is null.
     * @throws IllegalArgumentException if the password or hash is blank.
     * @throws NoSuchAlgorithmException if the algorithm is not found.
     */
    public static boolean checkPassword(String password, byte[] salt, String hash) throws NoSuchAlgorithmException, NullPointerException, IllegalArgumentException {
        Objects.requireNonNull(password, "Password cannot be null.");
        Objects.requireNonNull(salt, "Salt cannot be null.");
        Objects.requireNonNull(hash, "Hash cannot be null.");
        if (password.isBlank()) throw new IllegalArgumentException("Password cannot be blank.");
        if (hash.isBlank()) throw new IllegalArgumentException("Hash cannot be blank.");

        String newHash = generateHashedPassword(password, salt);

        return newHash.equals(hash);
    }
}
