package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class AddGoldAction.
 * A class that represents an action that increases the player's gold.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("add_gold")
public class AddGoldAction extends IntegerAction {
    
    /**
     * Constructor for AddGoldAction.
     * @param gold The gold that will be added to the player's gold.
     * @throws IllegalArgumentException if the gold is less than 0.
     */
    public AddGoldAction(int gold) throws IllegalArgumentException {
        super(gold);
    }

    /**
     * Empty constructor for JPA.
     */
    public AddGoldAction() {}


    /**
     * Method execute.
     * @param player The player that will have their gold increased.
     * @throws IllegalArgumentException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.addGold(this.getVal());
    }
}
