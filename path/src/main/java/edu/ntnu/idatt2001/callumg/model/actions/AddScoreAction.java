package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class AddScoreAction.
 * A class that represents an action that increases the player's score.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("add_score")
public class AddScoreAction extends IntegerAction {

    /**
     * Constructor for AddScoreAction.
     * @param points The points that will be added to the player's score.
     * @throws IllegalArgumentException if the points are less than 0.
     */
    public AddScoreAction(int points) throws NullPointerException {
        super(points);
    }

    /**
     * Empty constructor for JPA.
     */
    public AddScoreAction() {}

    /**
     * Method execute.
     * @param player The player that will have their score increased.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.addScore(this.getVal());
    }
}
