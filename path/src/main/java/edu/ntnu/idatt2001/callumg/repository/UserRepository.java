package edu.ntnu.idatt2001.callumg.repository;

import edu.ntnu.idatt2001.callumg.model.User;

/**
 * Class UserRepository.
 * @author Callum G.
 * @version 1.0 4.2.2023
 */
public class UserRepository extends AbstractRepository<User, String> {

    /**
     * Constructor for UserRepository.
     * @param entityManager The entity manager.
     * @param entityClass The entity class.
     * @throws NullPointerException if the entity manager or entity class is null.
     */
    public UserRepository() throws NullPointerException {
        super(User.class);
    }
}
