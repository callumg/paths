package edu.ntnu.idatt2001.callumg.model.actions;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;

@Entity
@Inheritance
@DiscriminatorColumn(name = "action_type")
@Table(name = "action")
public abstract class AbstractAction<T> implements Action<T> {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "action_id", nullable = false, updatable = false)
    private Long id;

    /**
     * Default constructor for JPA.
     */
    public AbstractAction() {}

    /**
     * Method getId.
     * @return The id of the action.
     */
    public Long getId() {
        return id;
    }

    /**
     * Method setId.
     * @param id The id of the action.
     * @throws IllegalArgumentException if the id is less than 0.
     */
    public void setId(Long id) throws IllegalArgumentException {
        if (id < 0) throw new IllegalArgumentException("Id cannot be negative.");
        this.id = id;
    }
}
