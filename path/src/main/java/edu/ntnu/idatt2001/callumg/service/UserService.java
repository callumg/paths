package edu.ntnu.idatt2001.callumg.service;

import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import org.hibernate.TransactionException;

import edu.ntnu.idatt2001.callumg.model.User;
import edu.ntnu.idatt2001.callumg.repository.UserRepository;
import edu.ntnu.idatt2001.callumg.utils.Authentication;

/**
 * Class UserService.
 * @author Callum G.
 * @version 1.3 14.2.2023
 */
public class UserService extends AbstractService<User, String> {
    /**
     * Constructor for UserService.
     * @throws NullPointerException if the entity manager or repository is null.
     */
    public UserService() throws NullPointerException {
        super(new UserRepository());
    }

    /**
     * Method log in.
     * @param username The username of the user.
     * @param password The password of the user.
     * @return The user that was logged in.
     * @throws IllegalArgumentException if the username or password is invalid.
     * @throws NullPointerException if the username or password is null.
     * @throws NoSuchAlgorithmException if the algorithm used to hash the password is not found.
     */
    public User logIn(String username, String password) throws IllegalArgumentException, NullPointerException, NoSuchAlgorithmException {
        Objects.requireNonNull(username, "The username cannot be null.");
        Objects.requireNonNull(password, "The password cannot be null.");
        if (username.isBlank()) throw new IllegalArgumentException("The username cannot be blank.");
        if (password.isBlank()) throw new IllegalArgumentException("The password cannot be blank.");

        User user = super.getById(username).orElseThrow(() -> new IllegalArgumentException("User does not exist."));

        if (!Authentication.checkPassword(password, user.getSalt(), user.getHashedPassword())) throw new IllegalArgumentException("Incorrect password.");

        UserStateService.setCurrentUser(user);

        return user;
    }

    /**
     * Method log out.
     * Set's the current user to null.
     */
    public static void logOut() {
        UserStateService.setCurrentUser(null);
    }
    
    /**
     * Adds a user to the repository.
     * @param username The username of the user.
     * @param password The password of the user.
     * @return The user that was added.
     * @throws IllegalArgumentException if the username or password is invalid.
     * @throws NullPointerException if the username or password is null.
     * @throws NoSuchAlgorithmException if the algorithm used to hash the password is not found.
     * @throws TransactionException if the transaction fails.
     */
    public User registerUser(String username, String password) throws NullPointerException, IllegalArgumentException, NoSuchAlgorithmException, TransactionException {
        Objects.requireNonNull(username, "The username cannot be null.");
        Objects.requireNonNull(password, "The password cannot be null.");
        if (username.isBlank()) throw new IllegalArgumentException("The username cannot be blank.");
        if (password.isBlank()) throw new IllegalArgumentException("The password cannot be blank.");

        byte[] salt = Authentication.generateSalt();
        String hashedPassword = Authentication.generateHashedPassword(password, salt);

        User user = new User(username, hashedPassword, salt);

        return super.add(user);
    }
    
    /**
     * Method to remove a user from the repository.
     * @param username The username of the user.
     * @return The user that was removed.
     * @throws IllegalArgumentException if the username is invalid.
     * @throws NullPointerException if the username is null.
     */
    @Override
    public User removeById(String username) throws NullPointerException, IllegalArgumentException, TransactionException {
        Objects.requireNonNull(username, "The username cannot be null.");
        if (username.isBlank()) throw new IllegalArgumentException("The username cannot be blank.");

        return super.removeById(username);
    }
}
