package edu.ntnu.idatt2001.callumg.model.goals;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Interface Goal.
 * An interface that represents a goal.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
public interface Goal<T> {
    /**
     * Method isCompleted.
     * @param player The player that will have their goal checked.
     * @return true if the player's goal is greater than or equal to the goal's goal.
     * @throws NullPointerException if the player is null.
     * @throws IllegalArgumentException if the player's goal is less than 0 or is blank.
     */
    public boolean isCompleted(Player player) throws NullPointerException, IllegalArgumentException; // interface method (does not have a body)

    /**
     * Method getGoalId.
     * @return the goal's id.
     */
    public Long getGoalId(); // interface method (does not have a body)

    /**
     * Method getVal.
     * @return the goal's value.
     */
    public T getVal(); // interface method (does not have a body)
}
