package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class RemoveHealthAction.
 * A class that represents an action that decreases the player's health.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("remove_health")
public class RemoveHealthAction extends IntegerAction {

    /**
     * Constructor for RemoveHealthAction.
     * @param health The health that will be removed from the player's health.
     * @throws IllegalArgumentException if the health is less than 0.
     */
    public RemoveHealthAction(int health) throws IllegalArgumentException {
        super(health);
    }

    /**
     * Empty constructor for JPA.
     */
    public RemoveHealthAction() {}

    /**
     * Method execute.
     * @param player The player that will have their health decreased.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.removeHealth(this.getVal());
    }
}
