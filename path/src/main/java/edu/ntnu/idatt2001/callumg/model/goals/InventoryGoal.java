package edu.ntnu.idatt2001.callumg.model.goals;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class InventoryGoal.
 * A class that represents an inventory goal.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("inventory_amount")
public class InventoryGoal extends IntegerGoal {
    
    /**
    * Constructor for InventoryGoal.
    * @param goal The goal that will be checked against the player's inventory.
    * @throws IllegalArgumentException if the goal is less than 0.
    */
    public InventoryGoal(int goal) throws NullPointerException, IllegalArgumentException {
        super(goal);
    }

    /**
     * Empty constructor for JPA
    */
    public InventoryGoal() {}

    /**
    * Method isCompleted.
    * @param player The player that will have their inventory checked.
    * @return true if the player's inventory is greater than or equal to the goal's inventory.
    * @throws NullPointerException if the player is null.
    */
    @Override
    public boolean isCompleted(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "Player cannot be null.");
        return player.getInventory().size() >= this.getVal();
    }
}
