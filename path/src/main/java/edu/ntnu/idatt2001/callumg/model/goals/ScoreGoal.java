package edu.ntnu.idatt2001.callumg.model.goals;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class ScoreGoal.
 * A class that represents a score goal.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("score")
public class ScoreGoal extends IntegerGoal {

    /**
     * Constructor for ScoreGoal.
     * @param goal The goal that will be checked against the player's score.
     * @throws IllegalArgumentException if the goal is less than 0.
     */
    public ScoreGoal(int goal) throws IllegalArgumentException, NullPointerException {
        super(goal);
    }

    /**
     * Empty constructor for JPA.
     */
    public ScoreGoal() {}

    /**
     * Method isCompleted.
     * @param player The player that will have their score checked.
     * @return true if the player's score is greater than or equal to the goal's score.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public boolean isCompleted(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "Player cannot be null.");
        return player.getScore() >= this.getVal();
    }    
}
