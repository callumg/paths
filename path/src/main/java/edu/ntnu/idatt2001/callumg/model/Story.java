package edu.ntnu.idatt2001.callumg.model;

import java.lang.IllegalArgumentException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/** Class Story.
 * A class that represents a story in the game.
 * @author Callum G.
 * @version 1.0 1.2.2022
 */

@Entity
@Table (name = "story")
public class Story {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "story_id", nullable = false, updatable = false)
    private Long id;
    
    @Column(name = "title", nullable = false)
    private String title;


    @MapKey(name = "id")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    @JoinTable(
        name = "story_passage", 
        joinColumns = { @JoinColumn(name = "story_id", referencedColumnName = "story_id") },
        inverseJoinColumns = { @JoinColumn(name = "passage_id", referencedColumnName = "passage_id") }
    )
    private Map<Link, Passage> passages;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "opening_passage", referencedColumnName = "passage_id")
    private Passage openingPassage;

    /**
     * Constructor for Story.
     * @param title The title of the story.
     * @param passages The passages in the story.
     * @param openingPassage The opening passage of the story.
     * @throws IllegalArgumentException if the title is blank or null.
     * @throws IllegalArgumentException if the opening passage is null.
     */
    public Story(String title, Passage openingPassage) throws IllegalArgumentException {
        if (title == null) throw new IllegalArgumentException("Title cannot be null.");
        if (title.isBlank()) throw new IllegalArgumentException("Title cannot be blank.");
        if (openingPassage == null) throw new IllegalArgumentException("Opening passage cannot be null.");
        this.title = title;
        this.openingPassage = openingPassage;
        this.passages = new HashMap<Link, Passage>();
    }

    /**
     * Default constructor for JPA.
     */
    public Story() {}

    /**
     * Method getId.
     * @return The id of the story.
     */
    public Long getId() {
        return this.id;
    }
    
    /**
     * Method getTitle.
     * @return The title of the story.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Method getOpeningPassage.
     * @return The opening passage of the story.
     */
    public Passage getOpeningPassage() {
        return this.openingPassage;
    }
    
    /**
     * Method addPassage.
     * @param passage The passage that will be added to the story.
     * @throws IllegalArgumentException if the passage is null.
     */
    public void addPassage(Passage passage) {
        if (passage == null) throw new IllegalArgumentException("Passage cannot be null.");
        this.passages.put(new Link(passage.getTitle(), passage.getTitle()), passage);
    }

    /**
     * Method getPassage.
     * @param link The link of the passage that will be returned.
     * @return The passage that corresponds to the link.
     * @throws IllegalArgumentException if the link is null.
     */
    public Passage getPassage(Link link) {
        if (link == null) throw new IllegalArgumentException("Link cannot be null.");
        return this.passages.get(link);
    }

    /**
     * Method getPassages.
     * @return The passages in the story.
     */
    public Collection<Passage> getPassages() {
        return this.passages.values();
    }

    /**
     * Method setId.
     * @param id The id of the story.
     * @throws IllegalArgumentException if the id is negative.
     */
    public void setId(Long id) {
        if (id < 0) throw new IllegalArgumentException("Id cannot be negative.");
        this.id = id;
    }

    /**
     * Method setOpeningPassage.
     * @param openingPassage The opening passage of the story.
     * @throws IllegalArgumentException if the opening passage is null.
     */
    public void setOpeningPassage(Passage openingPassage) {
        if (openingPassage == null) throw new IllegalArgumentException("Opening passage cannot be null.");
        this.openingPassage = openingPassage;
    }

    /**
     * Method setTitle.
     * @param title The title of the story.
     * @throws IllegalArgumentException if the title is blank or null.
     */
    public void setTitle(String title) {
        if (title == null) throw new IllegalArgumentException("Title cannot be null.");
        if (title.isBlank()) throw new IllegalArgumentException("Title cannot be blank.");
        this.title = title;
    }

    /**
     * Method setPassages.
     * @param passages The passages in the story.
     * @throws IllegalArgumentException if the passages is null.
     */
    public void setPassages(Collection<Passage> passages) {
        if (passages == null) throw new IllegalArgumentException("Passages cannot be null.");
        this.passages.clear();
        passages.forEach(passage -> this.passages.put(new Link(passage.getTitle(), passage.getTitle()), passage));
    }

    /**
     * Method setPassages.
     * @param passages The passages in the story.
     * @throws IllegalArgumentException if the passages is null.
     */
    public void setPassages(Map<Link, Passage> passages) {
        if (passages == null) throw new IllegalArgumentException("Passages cannot be null.");
        this.passages = passages;
    }
}
