package edu.ntnu.idatt2001.callumg.test;

import java.security.NoSuchAlgorithmException;

import org.hibernate.TransactionException;

import edu.ntnu.idatt2001.callumg.service.UserService;
import edu.ntnu.idatt2001.callumg.service.UserStateService;

public class UserDatabaseClient {
    public static void main(String[] args) {
        UserService userService = null;
        
        try {
            userService = new UserService();
        } catch (Exception e) {
            e.printStackTrace();  
        }

        System.out.println("User: " + UserStateService.getCurrentUser().getUsername());
        
        try {
            userService.registerUser("callum", "admin");
        } catch (IllegalArgumentException | NullPointerException | NoSuchAlgorithmException | TransactionException e) {
            e.printStackTrace();
        }

        try {
            userService.logIn("callum", "admin");
        } catch (IllegalArgumentException | NullPointerException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        System.out.println("User: " + UserStateService.getCurrentUser().getUsername());

        UserService.logOut();

        System.out.println("User: " + UserStateService.getCurrentUser().getUsername());
    }
}
