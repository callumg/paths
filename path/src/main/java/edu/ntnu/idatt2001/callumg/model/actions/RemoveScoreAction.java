package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class RemoveScoreAction.
 * A class that represents an action that decreases the player's score.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("remove_score")
public class RemoveScoreAction extends IntegerAction {

    /**
     * Constructor for RemoveScoreAction.
     * @param points The points that will be removed from the player's score.
     * @throws IllegalArgumentException if the points are less than 0.
     */
    public RemoveScoreAction(int points) throws IllegalArgumentException {
        super(points);
    }

    /**
     * Empty constructor for JPA.
     */
    public RemoveScoreAction() {}

    /**
     * Method execute.
     * @param player The player that will have their score decreased.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.removeScore(this.getVal());
    }
}
