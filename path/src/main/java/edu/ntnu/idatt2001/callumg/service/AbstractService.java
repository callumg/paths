package edu.ntnu.idatt2001.callumg.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.hibernate.TransactionException;

import edu.ntnu.idatt2001.callumg.repository.Repository;

/**
 * Class AbstractService.
 * @author Callum G.
 * @version 1.2 14.2.2023
 * @param <T> The type of the entity.
 * @param <ID> The type of the id.
 */
public abstract class AbstractService<T, ID> {
    private Repository<T, ID> repository;

    /**
     * Constructor for AbstractService.
     * @param repository The repository to use.
     */
    public AbstractService(Repository<T, ID> repository) throws NullPointerException {
        Objects.requireNonNull(repository, "The repository cannot be null.");
        this.repository = repository;
    }

    /**
     * Gets the repository.
     * @return Returns the repository.
     */
    public Repository<T, ID> getRepository() {
        return repository;
    }

    /**
     * Sets the repository.
     * @param repository The repository to set.
     * @throws NullPointerException if the repository is null.
     */
    public void setRepository(Repository<T, ID> repository) throws NullPointerException {
        Objects.requireNonNull(repository, "The repository cannot be null.");
        this.repository = repository;
    }

    /**
     * Adds an object to the repository.
     * @param object The object to add.
     * @throws NullPointerException if the object is null.
     * @throws IllegalArgumentException if the object already exists in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T add(T object) throws NullPointerException, IllegalArgumentException, TransactionException {
        return repository.add(object);
    }

    /**
     * Removes an object from the repository.
     * @param object The object to remove.
     * @throws NullPointerException if the object is null.
     * @throws IllegalArgumentException if the object does not exist in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public void remove(T object) throws NullPointerException, IllegalArgumentException, TransactionException {
        repository.remove(object);
    }

    /**
     * Removes an object from the repository by id.
     * @param id The id of the object to remove.
     * @throws NullPointerException if the ID is null.
     * @throws IllegalArgumentException if the object does not exist in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T removeById(ID id) throws NullPointerException, IllegalArgumentException, TransactionException {
        return repository.removeId(id);
    }

    /**
     * Gets all objects from the repository.
     * @return Returns all objects.
     * @throws NullPointerException if the object is null.
     * @throws IllegalArgumentException if the object does not exist in the repository.
     * @throws TransactionException if the transaction fails.
     */
    public T update(T object) throws NullPointerException, IllegalArgumentException, TransactionException {
        return repository.update(object);
    }

    /**
     * Gets an object from the repository by id.
     * @param id The id of the object to get.
     * @return Returns the object.
     * @throws NullPointerException if the id is null.
     */
    public Optional<T> getById(ID id) throws NullPointerException {
        return repository.find(id);
    }

    /**
     * Gets all objects from the repository.
     * @return All objects.
     */
    public List<T> getAll() {
        return repository.findAll();
    }

    /**
     * Gets the number of objects.
     * @return Returns the number of objects.
     * @throws IllegalArgumentException if the repository is empty.
     */
    public int getSize() throws IllegalArgumentException {
        return repository.size();
    }

    /**
     * Checks if the repository is empty.
     * @return Returns true if the repository is empty, false otherwise.
     * @throws IllegalArgumentException if the repository is empty.
     */
    public boolean isEmpty() throws IllegalArgumentException {
        return repository.isEmpty();
    }
    
    /**
     * Method deleteAll.
     * Deletes all of the items the repository.
     * @throws IllegalArgumentException if the repository is empty.
     * @throws TransactionException if the transaction fails.
     */
    public void deleteAll() throws IllegalArgumentException, TransactionException {
        repository.clear();
    }
}
