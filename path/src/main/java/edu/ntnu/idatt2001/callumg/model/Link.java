package edu.ntnu.idatt2001.callumg.model;

import java.lang.IndexOutOfBoundsException;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import edu.ntnu.idatt2001.callumg.model.actions.AbstractAction;
import edu.ntnu.idatt2001.callumg.model.actions.Action;

/**
 * Class Link.
 * A class that represents a link to an action.
 * @author Callum G.
 * @version 1.0 1.2.2022
 */

@Entity
@Table (name = "link")
public class Link {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "link_id", nullable = false, updatable = false)
    private Long id;
    
    @Column(name = "text", nullable = false)
    private String text;
    
    @Column(name = "reference", nullable = false)
    private String reference;

    @ManyToMany(cascade = CascadeType.ALL, targetEntity=AbstractAction.class)
    @JoinTable(
        name = "link_action", 
        joinColumns = { @JoinColumn(name = "link_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "action_id") }
    )
    private List<Action<?>> actions;
    
    /**
     * Constructor for Link.
     * @param text The text that will be displayed to the user.
     * @param reference The reference to the action.
     * @param actions The actions that will can be performed.
     * @throws IllegalArgumentException if the text or reference is blank or null.
     */
    public Link(String text, String reference) throws IllegalArgumentException {
        if (text == null) throw new IllegalArgumentException("Text cannot be null.");
        if (text.isBlank()) throw new IllegalArgumentException("Text cannot be blank.");
        if (reference == null) throw new IllegalArgumentException("Reference cannot be null.");
        if (reference.isBlank()) throw new IllegalArgumentException("Reference cannot be blank.");
        this.text = text;
        this.reference = reference;
        this.actions = new ArrayList<Action<?>>();
    }

    /**
     * Empty constructor for Link.
     */
    public Link() {}

    /**
     * Method getId.
     * @return The id of the link.
     */
    public Long getId() {
        return this.id;
    }

    /**
     *  Method getText.
     *  @return The text that will be displayed to the user.
     */
    public String getText() {
        return this.text;
    }

    /**
     *  Method getReference.
     *  @return The reference to the action.
     */
    public String getReference() {
        return this.reference;
    }

    /**
     * Method addAction.
     * @param action The action that will be added to the list of actions.
     * @throws IllegalArgumentException if the action is null.
     */
    public boolean addAction(Action<?> action) throws IllegalArgumentException {
        if (action == null) throw new IllegalArgumentException("Action cannot be null.");
        return this.actions.add(action);
    }

    /**
     * Method addAction.
     * @param action The action that will be added to the list of actions.
     * @param index The index of the action.
     * @throws IllegalArgumentException if the action is null.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public void addAction(Action<?> action, int index) throws IndexOutOfBoundsException, IllegalArgumentException {
        if (action == null) throw new IllegalArgumentException("Action cannot be null.");
        if (index < 0 || index >= this.actions.size()) throw new IndexOutOfBoundsException("Index out of bounds.");
        this.actions.add(index, action);
    }

    /**
     * Method removeAction.
     * @param action The action that will be removed from the list of actions.
     * @throws IllegalArgumentException if the action is null.
     */
    public boolean removeAction(Action<?> action) throws IllegalArgumentException {
        if (action == null) throw new IllegalArgumentException("Action cannot be null.");
        return this.actions.remove(action);
    }

    /**
     * Method removeAction.
     * @param index The index of the action that will be removed from the list of actions.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public void removeAction(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= this.actions.size()) throw new IndexOutOfBoundsException("Index out of bounds.");
        this.actions.remove(index);
    }

    /**
     * Method getAction.
     * @param index The index of the action.
     * @return The action at the index.
     */
    public Action<?> getAction(int index) {
        return this.actions.get(index);
    }

    /**
     * Method getActions.
     * @return The list of actions.
     */
    public List<Action<?>> getActions() {
        return this.actions;
    }

    /**
     * Method setId.
     * @param id The id of the link.
     * @throws IllegalArgumentException if the id is less than 0.
     */
    public void setId(Long id) throws IllegalArgumentException {
        if (id < 0) throw new IllegalArgumentException("ID cannot be less than 0.");
        this.id = id;
    }

    /**
     * Method setText.
     * @param text The text that will be displayed to the user.
     * @throws IllegalArgumentException if the text is blank or null.
     */
    public void setText(String text) throws IllegalArgumentException {
        if (text == null) throw new IllegalArgumentException("Text cannot be null.");
        if (text.isBlank()) throw new IllegalArgumentException("Text cannot be blank.");
        this.text = text;
    }

    /**
     * Method setReference.
     * @param reference The reference to the action.
     * @throws IllegalArgumentException if the reference is blank or null.
     */
    public void setReference(String reference) throws IllegalArgumentException {
        if (reference == null) throw new IllegalArgumentException("Reference cannot be null.");
        if (reference.isBlank()) throw new IllegalArgumentException("Reference cannot be blank.");
        this.reference = reference;
    }

    /**
     * Method setActions.
     * @param actions The actions that will can be performed.
     * @throws IllegalArgumentException if the actions is empty.
     */
    public void setActions(List<Action<?>> actions) throws IllegalArgumentException {
        if (actions == null) throw new IllegalArgumentException("Actions cannot be null.");
        this.actions = actions;
    }

    /**
     * Method setAction.
     * @param action The action that will be added to the list of actions.
     * @param index The index of the action.
     * @throws IllegalArgumentException if the action is null.
     */
    public void setAction(int index, Action<?> action) throws IllegalArgumentException {
        if (action == null) throw new IllegalArgumentException("Action cannot be null.");
        this.actions.set(index, action);
    }

    /**
     * Method toString.
     * @return The string representation of the link.
     */
    @Override
    public String toString() {
        return "Link{" +
                "text='" + text + '\'' +
                ", reference='" + reference + '\'' +
                ", actions=" + actions +
                '}';
    }

    /**
     * Method equals.
     * @param o The object that will be compared to the link.
     * @return True if the objects are equal, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return Objects.equals(this.text, link.text) && 
                Objects.equals(this.reference, link.reference) &&
                Objects.equals(this.actions, link.actions);
    }

    /**
     * Method hashCode.
     * @return The hash code of the link.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.text, this.reference, this.actions);
    }
}
