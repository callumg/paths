package edu.ntnu.idatt2001.callumg.model.goals;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class GoldGoal.
 * A class that represents a gold goal.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("gold")
public class GoldGoal extends IntegerGoal {

    /**
     * Constructor for GoldGoal.
     * @param goal The goal that will be checked against the player's gold.
     * @throws IllegalArgumentException if the goal is less than 0.
     */
    public GoldGoal(int goal) throws NullPointerException, IllegalArgumentException {
        super(goal);
    }

    /**
     * Empty constructor for JPA
    */
    public GoldGoal() {}

    /**
     * Method isCompleted.
     * @param player The player that will have their gold checked.
     * @return true if the player's gold is greater than or equal to the goal's gold.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public boolean isCompleted(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "Player cannot be null.");
        return player.getGold() >= this.getVal();
    }
}
