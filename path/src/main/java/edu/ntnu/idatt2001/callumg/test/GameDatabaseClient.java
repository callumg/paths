package edu.ntnu.idatt2001.callumg.test;

import java.util.ArrayList;
import java.util.List;

import edu.ntnu.idatt2001.callumg.model.Game;
import edu.ntnu.idatt2001.callumg.model.Passage;
import edu.ntnu.idatt2001.callumg.model.Player;
import edu.ntnu.idatt2001.callumg.model.Story;
import edu.ntnu.idatt2001.callumg.model.goals.Goal;
import edu.ntnu.idatt2001.callumg.model.goals.GoldGoal;
import edu.ntnu.idatt2001.callumg.model.goals.HealthGoal;
import edu.ntnu.idatt2001.callumg.service.GameService;

public class GameDatabaseClient {
    public static void main(String[] args) {
        List<Goal<?>> goals = new ArrayList<Goal<?>>();
        goals.add(new GoldGoal(10));
        goals.add(new HealthGoal(10));

        Passage passage = new Passage("Room 1", "You are in a room.");
        Player player = new Player("Callum", 10, 10, 10);
        Story story = new Story("Battle", passage);
        story.addPassage(passage);
        Game game = new Game(player, story, goals);

        try {
            GameService gameService = new GameService();
            gameService.add(game);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
