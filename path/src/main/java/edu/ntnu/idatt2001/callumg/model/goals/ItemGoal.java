package edu.ntnu.idatt2001.callumg.model.goals;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class ItemGoal.
 * A class that represents an item goal.
 * @author Callum G.
 * @version 1.0 3.2.2023
 */
@Entity
@DiscriminatorValue("item")
public class ItemGoal extends StringGoal {
    
    /**
    * Constructor for ItemGoal.
    * @param goal The goal that will be checked against the player's items.
    * @throws NullPointerException if the goal is null.
    * @throws IllegalArgumentException if the goal is empty.
    */
    public ItemGoal(String goal) throws NullPointerException, IllegalArgumentException {
        super(goal);
    }

    /**
     * Empty constructor for JPA
    */
    public ItemGoal() {}

    /**
    * Method isCompleted.
    * @param player The player that will have their items checked.
    * @return true if the player's items contains the goal's item.
    * @throws NullPointerException if the player is null.
    */
    @Override
    public boolean isCompleted(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "Player cannot be null.");
        return player.getInventory().contains(this.getVal());
    }   
}
