package edu.ntnu.idatt2001.callumg.model.actions;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import edu.ntnu.idatt2001.callumg.model.Player;

/**
 * Class RemoveInventoryItemAction.
 * A class that represents an action that removes an inventory item from the player's inventory.
 * @author Callum G.
 * @version 1.0 2.2.2022
 */
@Entity
@DiscriminatorValue("remove_item")
public class RemoveInventoryItemAction extends StringAction {

    /**
     * Constructor for RemoveInventoryItemAction.
     * @param inventoryItem The inventory item that will be removed from the player's inventory.
     * @throws NullPointerException if the inventory item is null.
     * @throws IllegalArgumentException if the inventory item is empty.
     */
    public RemoveInventoryItemAction(String inventoryItem) throws NullPointerException, IllegalArgumentException {
        super(inventoryItem);
    }

    /**
     * Empty constructor for JPA.
     */
    public RemoveInventoryItemAction() {}

    /**
     * Method execute.
     * @param player The player that will have an item removed from their inventory.
     * @throws NullPointerException if the player is null.
     */
    @Override
    public void execute(Player player) throws NullPointerException {
        Objects.requireNonNull(player, "The player cannot be null.");
        player.removeFromInventory(this.getVal());
    }
}
