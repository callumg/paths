package edu.ntnu.idatt2001.callumg.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Class EntityManageSingleton.
 * @author Callum G.
 * @version 1.0 5.2.2023
 */
public class EntityManagerSingleton {
    
    private static EntityManagerFactory factory;
    
    // Static block to initialize the entity manager.
    static {
        factory = Persistence.createEntityManagerFactory("path");
    }
    
    /**
     * Method to get the entity manager.
     * @return The entity manager.
     */
    public static EntityManager getEntityManager() {
        return factory.createEntityManager();
    }
}
