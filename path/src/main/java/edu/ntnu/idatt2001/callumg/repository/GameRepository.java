package edu.ntnu.idatt2001.callumg.repository;

import edu.ntnu.idatt2001.callumg.model.Game;

public class GameRepository extends AbstractRepository<Game, Long> {
    public GameRepository() {
        super(Game.class);
    }
}
